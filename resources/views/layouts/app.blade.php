<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}
    <!-- Styles -->
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.siteKey') }}"></script>
</head>
<body>
<!-- PAGE -->
<main class="page" id="app">
    <!-- HEADER -->
    <header class="header">
        <div class="container">
            <div class="header-top">
                <div class="header-top__column">
                    <div class="header-logo">
                        <a href="{{ url('/') }}" class="header-logo__main">
                            <img src="{{ asset('assets/images/logo.svg') }}" alt="Pansust">
                        </a>
                        <a href="{{ url('/') }}" class="header-logo__nu">
                            <img src="{{ asset('assets/images/logo-nu.svg') }}" alt="Nazarbayev University">
                        </a>
                    </div>
                </div>
                <div class="header-top__column">
                    <div class="header-nav-wrapper">
                        <nav class="header-nav">
                            <ul class="nav-list">
                                <li class="nav-list__item {{ request()->is('about') ? 'is-active' : '' }}">
                                    <a href="{{ route('about') }}" class="nav-list__link">About us</a>
                                </li>
                                <li class="nav-list__item {{ request()->is('news*') ? 'is-active' : '' }}">
                                    <a href="{{ route('news.index') }}" class="nav-list__link">News</a>
                                </li>
                                <li class="nav-list__item {{ request()->is('events*') ? 'is-active' : '' }}">
                                    <a href="{{ route('events.index') }}" class="nav-list__link">Events</a>
                                </li>
                                <li class="nav-list__item {{ request()->is('wiki*') ? 'is-active' : '' }}">
                                    <a href="{{ route('glossaries.index') }}" class="nav-list__link">Pansust Wikipedia</a>
                                </li>
                                <li class="nav-list__item {{ request()->is('forum*') ? 'is-active' : '' }}">
                                    <a href="{{ route('forum.index') }}" class="nav-list__link">Forum</a>
                                </li>
                                <li class="nav-list__item {{ request()->is('sat*') ? 'is-active' : '' }}">
                                    <a href="{{ route('survey.index') }}" class="nav-list__link">Pansust sat</a>
                                </li>
                                <li class="nav-list__item {{ request()->is('contacts') ? 'is-active' : '' }}">
                                    <a href="{{ route('contacts') }}" class="nav-list__link">Contact us</a>
                                </li>
                            </ul>
                        </nav>
                        <div class="header-subscribe">
                            <button class="button button-primary" data-toggle="modal" data-target="#modal-subscribe">Subscribe</button>
                        </div>
                    </div>
                    <button class="mobile-nav-toggle hamburger hamburger--squeeze" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="header-bottom">
                <div class="header-search">
                    <form action="{{ route('search.index') }}" class="header-search__form">
                        <label class="header-search__icon" for="search-input"></label>
                        <input type="text"
                               name="query"
                               class="input header-search__input"
                               id="search-input"
                               value="{{ request()->get('query') ?? '' }}"
                        >
                    </form>
                </div>
                <div class="header-socials">
                    <div class="social-list">
                        <a href="{{ setting('social_link_instagram') }}" class="social-list__item instagram" target="_blank"></a>
                        <a href="{{ setting('social_link_facebook') }}" class="social-list__item facebook" target="_blank"></a>
                        <a href="{{ setting('social_link_linkedin') }}" class="social-list__item linkedin" target="_blank"></a>
                    </div>
                </div>
            </div>
            <div class="header-mobile-box">
                <nav class="header-mobile-box__nav">
                    <ul class="nav-list">
                        <li class="nav-list__item {{ request()->routeIs('home') ? 'is-active' : '' }}">
                            <a href="{{ route('home') }}" class="nav-list__link">Home</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('about') ? 'is-active' : '' }}">
                            <a href="{{ route('about') }}" class="nav-list__link">About us</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('news*') ? 'is-active' : '' }}">
                            <a href="{{ route('news.index') }}" class="nav-list__link">News</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('events*') ? 'is-active' : '' }}">
                            <a href="{{ route('events.index') }}" class="nav-list__link">Events</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('wiki*') ? 'is-active' : '' }}">
                            <a href="{{ route('glossaries.index') }}" class="nav-list__link">Pansust Wikipedia</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('forum*') ? 'is-active' : '' }}">
                            <a href="{{ route('forum.index') }}" class="nav-list__link">Forum</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('sat*') ? 'is-active' : '' }}">
                            <a href="{{ route('survey.index') }}" class="nav-list__link">Pansust sat</a>
                        </li>
                        <li class="nav-list__item {{ request()->is('contacts') ? 'is-active' : '' }}">
                            <a href="{{ route('contacts') }}" class="nav-list__link">Contact us</a>
                        </li>
                    </ul>
                </nav>
                <div class="header-mobile-box__subscribe">
                    <a href="#" data-toggle="modal" data-target="#modal-subscribe">Subscribe</a> to our news
                </div>
            </div>
        </div>
    </header>
    <!-- MAIN -->
    <div class="page-inner">
        @yield('content')
    </div>
    <!-- FOOTER -->
    <footer class="footer">
        <div class="container">
            <div class="footer-row">
                <div class="footer-row__column">
                    <div class="footer-subscribe">
                        <h3 class="footer-subscribe__title">Subscribe to our newsletter</h3>
                        <button class="button button-outline footer-subscribe__button" data-toggle="modal" data-target="#modal-subscribe">Fill out the form</button>
                    </div>
                </div>
                <div class="footer-row__column">
                    <div class="footer-content">
                        <div class="footer-content__column">
                            <div class="footer-contacts">
                                <div class="footer-contacts__row">
                                    <div class="footer-contacts__row-label">PRIMARY EMAIL:</div>
                                    <div class="footer-contacts__row-value">
                                        {{ setting('contacts_primary_email') }}
                                    </div>
                                </div>
                                <div class="footer-contacts__row">
                                    <div class="footer-contacts__row-label">PI’S EMAILS:</div>
                                    <div class="footer-contacts__row-value">
                                        @php
                                            $piEmails = explode(',', setting('contacts_pi_emails'));
                                        @endphp
                                        @foreach($piEmails as $email)
                                            <span>{{ $email }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="footer-socials social-list">
                                <a href="#" class="social-list__item instagram"></a>
                                <a href="#" class="social-list__item facebook"></a>
                                <a href="#" class="social-list__item linkedin"></a>
                            </div>
                        </div>
                        <div class="footer-content__column">
                            <div class="footer-nav">
                                <ul class="footer-nav__list">
                                    <li class="footer-nav__item">
                                        <a href="{{ route('about') }}" class="footer-nav__link">About us</a>
                                    </li>
                                    <li class="footer-nav__item">
                                        <a href="{{ route('news.index') }}" class="footer-nav__link">News</a>
                                    </li>
                                    <li class="footer-nav__item">
                                        <a href="{{ route('events.index') }}" class="footer-nav__link">Events</a>
                                    </li>
                                    <li class="footer-nav__item">
                                        <a href="{{ route('forum.index') }}" class="footer-nav__link">Forum</a>
                                    </li>
                                </ul>
                                <ul class="footer-nav__list">
                                    <li class="footer-nav__item">
                                        <a href="{{ route('glossaries.index') }}" class="footer-nav__link">Pansust wikipedia</a>
                                    </li>
                                    <li class="footer-nav__item">
                                        <a href="{{ route('survey.index') }}" class="footer-nav__link">Pansust sat</a>
                                    </li>
                                    <li class="footer-nav__item">
                                        <a href="{{ route('contacts') }}" class="footer-nav__link">Contact us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer-socials social-list">
                                <a href="{{ setting('social_link_instagram') }}" class="social-list__item instagram" target="_blank"></a>
                                <a href="{{ setting('social_link_facebook') }}" class="social-list__item facebook" target="_blank"></a>
                                <a href="{{ setting('social_link_linkedin') }}" class="social-list__item linkedin" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                &copy; Copyright 2021 Pansust
            </div>
        </div>
    </footer>
</main>
<!-- Modal -->
<div class="modal fade modal-subscribe" id="modal-subscribe" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body modal-message">
                <div class="success-template">
                    <div class="modal-message__icon success-icon"></div>
                    <h4 class="modal-message__title">Success!</h4>
                    <div class="modal-message__text">
                        Application successfully saved. Thank you for your subscription.
                    </div>
                </div>
                <h4 class="modal-message__title">Subscribe!</h4>
                <div class="modal-message__text">
                    Fill out the subscription form.
                </div>
                <form action="{{ route('feedback.store') }}" class="feedback-form ajax-form">
                    @csrf
                    <input type="hidden" name="formName" value="Subscribe">
                    <input type="hidden" name="type" value="subscribe">
                    <div class="input-block">
                        <input type="text" name="name" class="input" placeholder="Name">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block">
                        <input type="text" name="occupancy" class="input" placeholder="Occupancy">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block">
                        <input type="text" name="affiliation" class="input" placeholder="Affiliation">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block">
                        <input type="text" name="email" class="input" placeholder="E-mail">
                        <div class="input-error"></div>
                    </div>
                    <input type="hidden" name="recaptcha" class="recaptcha_response" value="">
                    <div class="input-error" id="recaptcha-error"></div>
                    <div class="input-block feedback-form__submit">
                        <button class="button button-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-success" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body modal-message">
                <div class="modal-message__icon success-icon"></div>
                <h4 class="modal-message__title">Success!</h4>
                <div class="modal-message__text">
                    Application successfully saved. Thank you for your feedback.
                </div>
                <button type="button" class="button button-primary" data-dismiss="modal">Continue</button>
                <div>
                    <a href="{{ route('home') }}" class="button button-link modal-message__link">Go to main</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-fail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body modal-message">
                <div class="modal-message-wrapper">
                    <div class="modal-message__icon fail-icon"></div>
                    <h4 class="modal-message__title">Error!</h4>
                    <div class="modal-message__text">
                        Something went wrong. Please try again or go to main page.
                    </div>
                    <button type="button" class="button button-primary" data-dismiss="modal">Continue</button>
                    <div>
                        <a href="{{ route('home') }}" class="button button-link modal-message__link">Go to main</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('assets/js/app.js') }}"></script>
<script>
    setTimeout(function () {
        grecaptcha.ready(function() {
            grecaptcha
                .execute("{{ config('services.recaptcha.siteKey') }}", {action: "{{ request()->segment(1) }}"})
                .then(function (token) {
                    if (token && document.querySelectorAll('.recaptcha_response')) {
                        document
                            .querySelectorAll('.recaptcha_response')
                            .forEach(elem => (elem.value = token));
                    }
                });
        });
    }, 100);
</script>
</body>
</html>
