@extends('layouts.app')

@section('content')
    <div class="page-wrapper contacts-page-wrapper">
        <div class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">Contacts</h1>
            </div>
            <section class="section section-hero page-hero">
                <div class="section-inner">
                    <h2 class="hero-content">
                        Send us a Message<br>
                        We’d love to hear from you!
                    </h2>
                </div>
            </section>
            <div class="contacts-wrapper">
                <div class="row">
                    <div class="column-md-7">
                        <div class="contacts-content">
                            <div class="contact-block">
                                <h4 class="contact-block__title">YOU CAN SEND AN EMAIL TO PANSUST BY FOLLOWING THE CONTACT LINKS:</h4>
                                <div class="contact-block__content">
                                    <div class="contact-block__content-row">
                                        <div class="contact-block__content-row-title">
                                            PRIMARY EMAIL:
                                        </div>
                                        <div class="contact-block__content-row-value">
                                            {{ setting('contacts_primary_email') }}
                                        </div>
                                    </div>
                                    <div class="contact-block__content-row">
                                        <div class="contact-block__content-row-title">
                                            PI’S EMAILS:
                                        </div>
                                        <div class="contact-block__content-row-value">
                                            @php
                                                $piEmails = explode(',', setting('contacts_pi_emails'));
                                            @endphp
                                            @foreach($piEmails as $email)
                                                <p>{{ $email }}</p>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contact-block">
                                <h4 class="contact-block__title">OUR SOCIAL NETWORKS:</h4>
                                <div class="contact-block__content">
                                    <div class="social-list">
                                        <a href="{{ setting('social_link_instagram') }}" class="social-list__item instagram" target="_blank"></a>
                                        <a href="{{ setting('social_link_facebook') }}" class="social-list__item facebook" target="_blank"></a>
                                        <a href="{{ setting('social_link_linkedin') }}" class="social-list__item linkedin" target="_blank"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="contact-block contact-block--location">
                                <div class="contact-block-info">
                                    <h4 class="contact-block__title">WE ARE LOCATED</h4>
                                    <div class="contact-block__content">
                                        <div class="contact-block__content-row">
                                            <div class="contact-block__content-row-title">
                                                Country:
                                            </div>
                                            <div class="contact-block__content-row-value">
                                                Republic of Kazakhstan
                                            </div>
                                        </div>
                                        <div class="contact-block__content-row">
                                            <div class="contact-block__content-row-title">
                                                CITY:
                                            </div>
                                            <div class="contact-block__content-row-value">
                                                Nur-Sultan
                                            </div>
                                        </div>
                                        <div class="contact-block__content-row">
                                            <div class="contact-block__content-row-title">
                                                ADDRESS:
                                            </div>
                                            <div class="contact-block__content-row-value">
                                                Kabanbay batyra street 53
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-block-map contacts-map contacts-map--mobile">
                                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A09d322d999f75b1f48c5d05b9727a8363d7432bc730c4a9b724f21492fabd208&amp;source=constructor" width="100%" height="300" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column-md-5">
                        <div class="contacts-map contacts-map--desktop">
                            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A09d322d999f75b1f48c5d05b9727a8363d7432bc730c4a9b724f21492fabd208&amp;source=constructor" width="100%" height="620" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <section class="section section-feedback-form">
                <h3 class="section-title">Keep in touch</h3>
                <form action="{{ route('feedback.store') }}" class="feedback-form ajax-form">
                    @csrf
                    <input type="hidden" name="formName" value="Keep in touch">
                    <div class="input-block">
                        <input type="text" name="name" class="input" placeholder="Name">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block">
                        <input type="text" name="occupancy" class="input" placeholder="Occupancy">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block">
                        <input type="text" name="affiliation" class="input" placeholder="Affiliation">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block">
                        <input type="text" name="email" class="input" placeholder="E-mail">
                        <div class="input-error"></div>
                    </div>
                    <div class="input-block feedback-form__textarea">
                        <textarea class="input" name="comment" rows="5" placeholder="Write a comment..."></textarea>
                        <div class="input-error"></div>
                    </div>
                    <input type="hidden" name="recaptcha" class="recaptcha_response" value="">
                    <div class="input-error" id="recaptcha-error"></div>
                    <div class="input-block feedback-form__submit">
                        <button class="button button-primary">Send</button>
                    </div>
                </form>
            </section>
        </div>
    </div>
@endsection
