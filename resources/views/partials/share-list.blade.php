@if(isset($onlyButtons) && $onlyButtons == true)
    <a href="#" class="social-list__item facebook share-button" data-type="fb"></a>
    <a href="#" class="social-list__item linkedin share-button" data-type="linkedin"></a>
@else
    <div class="social-list">
        <a href="#" class="social-list__item facebook share-button" data-type="fb"></a>
        <a href="#" class="social-list__item linkedin share-button" data-type="linkedin"></a>
    </div>
@endif
