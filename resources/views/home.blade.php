@extends('layouts.app')

@section('content')
    <section class="section section-hero">
        <div class="container">
            <div class="section-inner">
                <h1 class="hero-content">
                    {{ setting('homeTitle') }}
                </h1>
                <div class="hero-action">
                    <a href="{{ route('about') }}" class="button button-primary hero-button">Read more</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div style="max-width: 800px; margin: 0 auto">
                <iframe width="100%" height="400"
                        src="https://www.youtube-nocookie.com/embed/{{ setting('homeVideoUrl') }}?modestbranding=1&howinfo=0&rel=0"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </section>
    <section class="section section-news">
        <div class="container">
            <div class="section-inner">
                <div class="section-title-wrapper section-title-linked">
                    <h3 class="section-title">Latest news</h3>
                    <a href="{{ route('news.index') }}" class="section-all-link">See all news</a>
                </div>
                <div class="articles-list">
                    @foreach($news as $newsItem)
                        <div class="article-list__item">
                            <div class="article-list__item-inner">
                                <div class="article-list__item-preview">
                                    <a href="{{ route('news.show', $newsItem->slug) }}">
                                        <img src="{{ $newsItem->getPreviewImage() }}" alt="{{ $newsItem->title }}">
                                    </a>
                                </div>
                                <div class="article-list__item-published">{{ $newsItem->createdAtFormatted() }}</div>
                                <div class="article-list__item-title">
                                    <a href="{{ route('news.show', $newsItem->slug) }}" class="article-list__item-link">
                                        {{ $newsItem->title }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="section-footer">
                    <a href="{{ route('news.index') }}" class="button button-primary">See all news</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-events">
        <div class="container">
            <div class="section-inner">
                <div class="section-title-wrapper section-title-linked">
                    <h3 class="section-title">Latest events</h3>
                    <a href="{{ route('events.index') }}" class="section-all-link">See all events</a>
                </div>
                <div class="articles-list">
                    @foreach($events as $event)
                        <div class="article-list__item">
                            <div class="article-list__item-inner">
                                <div class="article-list__item-preview">
                                    <a href="{{ route('events.show', $event->slug) }}">
                                        <img src="{{ $event->getPreviewImage() }}" alt="{{ $event->title }}">
                                    </a>
                                </div>
                                <div class="article-list__item-published">{{ $event->createdAtFormatted() }}</div>
                                <div class="article-list__item-title">
                                    <a href="{{ route('events.show', $event->slug) }}" class="article-list__item-link">
                                        {{ $event->title }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="section-footer">
                    <a href="{{ route('events.index') }}" class="button button-primary">See all events</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-subscribe">
        <div class="container">
            <div class="section-inner">
                <h3 class="section-title">
                    Do you want to become a member of PANSUST and <a href="#" data-toggle="modal" data-target="#modal-subscribe">subscribe</a> to our
                    news?
                </h3>
            </div>
        </div>
    </section>
    <section class="section section-forum">
        <div class="container">
            <div class="section-inner">
                <div class="section-title-wrapper section-title-linked">
                    <h3 class="section-title">Forum</h3>
                    <a href="{{ route('forum.index') }}" class="section-all-link">See all topics</a>
                </div>
                <div class="articles-list articles-list--2column">
                    @foreach($forums as $forumItem)
                        <div class="article-list__item">
                            <div class="article-list__item-inner">
                                <div class="article-list__item-preview">
                                    <a href="{{ route('forum.show', $forumItem->slug) }}">
                                        <img src="{{ $forumItem->getThumbnailImage() }}" alt="{{ $forumItem->title }}">
                                    </a>
                                </div>
                                <div class="article-list__item-content">
                                    <div class="article-list__item-published">{{ $forumItem->createdAtFormatted() }}</div>
                                    <div class="article-list__item-title">
                                        <a href="{{ route('forum.show', $forumItem->slug) }}" class="article-list__item-link">
                                            {{ $forumItem->title }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="section-footer">
                    <a href="{{ route('forum.index') }}" class="button button-primary">See all topics</a>
                </div>
            </div>
        </div>
    </section>
@endsection
