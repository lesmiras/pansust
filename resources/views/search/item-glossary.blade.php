<div class="wiki-list__item">
    <div class="wiki-list__item-header">
        <h4 class="wiki-list__item-title">{{ $item->title }}</h4>
    </div>
    <div class="wiki-list__item-body">
        <div class="wiki-list__item-text">
            {!! $item->content !!}
        </div>
    </div>
</div>