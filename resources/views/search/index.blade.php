@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper page-title-no-border">
                <h1 class="page-title">Search results</h1>
            </div>
            <div class="tabs-nav-wrapper">
                <ul class="tabs-nav" data-tab-nav="search">
                    <li class="tabs-nav__item" data-tab-item>
                        <a href="#main" class="tabs-nav__link">All ({{ $total }})</a>
                    </li>
                    @if(isset($news) && $news->total())
                        <li class="tabs-nav__item" data-tab-item>
                            <a href="#tab-news" class="tabs-nav__link">News ({{ $news->total() }})</a>
                        </li>
                    @endif
                    @if(isset($events) && $events->total())
                        <li class="tabs-nav__item" data-tab-item>
                            <a href="#tab-events" class="tabs-nav__link">Event ({{ $events->total() }})</a>
                        </li>
                    @endif
                    @if(isset($forums) && $forums->total())
                        <li class="tabs-nav__item" data-tab-item>
                            <a href="#tab-forum" class="tabs-nav__link">Forum ({{ $forums->total() }})</a>
                        </li>
                    @endif
                    @if(isset($glossaries) && $glossaries->total())
                        <li class="tabs-nav__item" data-tab-item>
                            <a href="#tab-glossaries" class="tabs-nav__link">Pansust wikipedia ({{ $glossaries->total() }})</a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="search-results-title">
                {{ $total }} RESULTS FOUND ON REQUEST <span>{{ $searchQuery }}</span>
            </div>
            <div class="tabs-content search-results-wrapper" data-tab-content="search">
                @if(isset($news) && $news->total())
                    <div class="tab-panel" id="tab-news" data-tab-panel>
                        <div class="search-result-items">
                            @foreach($news as $newsItem)
                                @include('search.item-article', ['item' => $newsItem])
                            @endforeach
                        </div>
                        @if($news->total() > $news->perPage())
                            <div class="search-result-more">
                                <button class="button button-primary"
                                        data-load-search="news"
                                        data-query="{{ $searchQuery }}"
                                        data-page="1"
                                >
                                    Load more
                                </button>
                            </div>
                        @endif
                        <div class="search-result-divider"></div>
                    </div>
                @endif
                @if(isset($events) && $events->total())
                    <div class="tab-panel" id="tab-events" data-tab-panel>
                        <div class="search-result-items">
                            @foreach($events as $event)
                                @include('search.item-article', ['item' => $event])
                            @endforeach
                        </div>
                        @if($events->total() > $events->perPage())
                            <div class="search-result-more">
                                <button class="button button-primary"
                                        data-load-search="events"
                                        data-query="{{ $searchQuery }}"
                                        data-page="1"
                                >
                                    Load more
                                </button>
                            </div>
                        @endif
                        <div class="search-result-divider"></div>
                    </div>
                @endif
                @if(isset($forums) && $forums->total())
                    <div class="tab-panel" id="tab-forum" data-tab-panel>
                        <div class="search-result-items">
                            @foreach($forums as $forumItem)
                                @include('search.item-article', ['item' => $forumItem])
                            @endforeach
                        </div>
                        @if($forums->total() > $forums->perPage())
                            <div class="search-result-more">
                                <button class="button button-primary"
                                        data-load-search="forum"
                                        data-query="{{ $searchQuery }}"
                                        data-page="1"
                                >
                                    Load more
                                </button>
                            </div>
                        @endif
                        <div class="search-result-divider"></div>
                    </div>
                @endif
                @if(isset($glossaries) && $glossaries->total())
                    <div class="tab-panel" id="tab-glossaries" data-tab-panel>
                        <div class="search-result-items">
                            @foreach($glossaries as $glossary)
                                @include('search.item-glossary', ['item' => $glossary])
                            @endforeach
                        </div>
                        @if($glossaries->total() > $glossaries->perPage())
                            <div class="search-result-more">
                                <button class="button button-primary"
                                        data-load-search="glossaries"
                                        data-query="{{ $searchQuery }}"
                                        data-page="1"
                                >
                                    Load more
                                </button>
                            </div>
                        @endif
                        <div class="search-result-divider"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
