<div class="result-item">
    <div class="result-item-inner">
        <div class="result-item__preview">
            <a href="{{ $item->getPublicLink() }}">
                <img src="{{ $item->getThumbnailImage() }}" alt="{{ $item->title }}">
            </a>
        </div>
        <div class="result-item__content">
            <h3 class="result-item__title">
                <a href="{{ $item->getPublicLink() }}" class="result-item__link">
                    {{ $item->title }}
                </a>
            </h3>
            <div class="result-item__description">
                {{ $item->excerpt }}
            </div>
        </div>
    </div>
</div>