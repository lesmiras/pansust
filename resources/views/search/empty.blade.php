@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper page-title-no-border">
                <h1 class="page-title">Search results</h1>
            </div>
            <div class="search-empty-wrapper">
                <div class="search-empty-inner">
                    <div class="search-empty__image">
                        <img src="{{ asset('assets/images/search-empty.png') }}" alt="Pansust">
                    </div>
                    <div class="search-empty__title">No Results To Show</div>
                    <a href="{{ route('home') }}" class="button button-primary">Go home</a>
                </div>
            </div>
        </div>
    </div>
@endsection
