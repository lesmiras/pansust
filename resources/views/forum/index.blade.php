@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper page-title-linked">
                <h1 class="page-title">Forum</h1>
                @if(request()->get('order') === 'popular')
                    <a href="{{ route('forum.index') }}" class="page-title-link button-sort">Latest topics</a>
                @else
                    <a href="{{ route('forum.index', ['order' => 'popular']) }}" class="page-title-link button-sort">Popular topics</a>
                @endif
            </div>
            <div class="article-blocks">
                @foreach($forums as $forumItem)
                    @include('forum.partial.article-block', ['article' => $forumItem])
                @endforeach
            </div>
            @if($forums->lastPage() > 1)
                <div class="page-load-more">
                    <button class="button button-primary"
                            data-load-articles="forum"
                            data-page="1"
                            data-order="{{ request()->get('order') }}"
                    >
                        Load more
                    </button>
                </div>
            @endif
        </div>
    </div>
@endsection
