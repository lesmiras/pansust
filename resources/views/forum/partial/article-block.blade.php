<div class="article-blocks__item">
    <div class="article-blocks__item-preview">
        <a href="{{ route('forum.show', $article->slug) }}">
            <img src="{{ $article->getPreviewImage() }}" alt="{{ $article->title }}">
        </a>
    </div>
    <div class="article-blocks__item-published">{{ $article->createdAtFormatted() }}</div>
    <div class="article-blocks__item-title">
        <a href="{{ route('forum.show', $article->slug) }}" class="article-blocks__item-link">
            {{ $article->title }}
        </a>
    </div>
</div>