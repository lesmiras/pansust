@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">{{ $article->title }}</h1>
                <div class="page-title-published">{{ $article->createdAtFormatted() }}</div>
            </div>
            <div class="article-content">
                <div class="article-content-text">
                    {!! $article->content !!}
                </div>
                <section class="section-share">
                    <div class="share-wrapper">
                        <div class="share-title">SHARE THIS</div>
                        @include('partials.share-list')
                    </div>
                </section>
            </div>
            <section class="section section-comment-form">
                <comment-form id="{{ $article->id }}" type="forum"></comment-form>
            </section>
            <section class="section section-comment-list">
                @if (count($comments))
                    <div class="comment-list">
                        @foreach($comments as $comment)
                            @include('comments.comment-block', ['comment' => $comment])
                        @endforeach
                    </div>
                    @if($comments->lastPage() > 1)
                        <div class="comments-load-more">
                            <button class="button button-primary"
                                    data-load-comments="forum"
                                    data-id="{{ $article->id }}"
                                    data-page="1"
                            >
                                Load more
                            </button>
                        </div>
                    @endif
                @endif
            </section>
            @if ($relatedArticles)
                <section class="section section-related-articles">
                    <div class="section-title-wrapper">
                        <h3 class="section-title">Read more</h3>
                    </div>
                    <div class="article-blocks">
                        @foreach($relatedArticles as $relatedArticle)
                            @include('forum.partial.article-block', ['article' => $relatedArticle])
                        @endforeach
                    </div>
                </section>
            @endif
        </div>
    </div>
@endsection
