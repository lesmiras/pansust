@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper page-title-linked">
                <h1 class="page-title">News</h1>
                @if(request()->get('order') === 'popular')
                    <a href="{{ route('news.index') }}" class="page-title-link button-sort">Latest news</a>
                @else
                    <a href="{{ route('news.index', ['order' => 'popular']) }}" class="page-title-link button-sort">Popular news</a>
                @endif
            </div>
            <div class="article-blocks">
                @foreach($news as $newsItem)
                    @include('news.partial.article-block', ['article' => $newsItem])
                @endforeach
            </div>
            @if($news->lastPage() > 1)
                <div class="page-load-more">
                    <button class="button button-primary"
                            data-load-articles="news"
                            data-page="1"
                            data-order="{{ request()->get('order') }}"
                    >
                        Load more
                    </button>
                </div>
            @endif
        </div>
    </div>
@endsection
