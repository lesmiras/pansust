@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Bids management</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Bids</li>
                    <li class="breadcrumb-item">List</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Bids</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Content</th>
                            <th>Status</th>
                            <th style="width: 200px;">Created at</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bids as $bid)
                            <tr>
                                <th class="text-center">{{ $bid->id }}</th>
                                <td class="font-w600">{{ $bid->name }}</td>
                                <td>{{ $bid->type }}</td>
                                <td>
                                    {!! implode($bid->body, '<br>') !!}
                                </td>
                                <td>{!! $bid->getStatusBadge() !!}</td>
                                <td>{{ $bid->created_at }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.bids.edit', $bid) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $bids->links() }}
            </div>
        </div>
    </div>
@endsection
