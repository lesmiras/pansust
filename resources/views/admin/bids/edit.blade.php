@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Bids management
                <small class="d-block d-sm-inline-block font-size-base font-w400 text-muted">
                    {{ $bid->title }}
                </small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.bids.index') }}">Bids</a>
                    </li>
                    <li class="breadcrumb-item">Edit</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-9">
                {{ Form::model($bid, ['route' => ['admin.bids.update', $bid->id], 'method' => 'put', 'files' => true]) }}
                <div class="block">
                    <div class="block-header">
                        <div class="block-options">
                            <a href="{{ route('admin.bids.index') }}" class="btn btn-sm btn-outline-secondary">
                                <i class="fa fa-fw fa-angle-left"></i>
                                Back to list
                            </a>
                        </div>
                        <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-primary btn-save">
                                <i class="fa fa-fw fa-save"></i>
                                Save
                            </button>
                            <button type="button"
                                    class="btn btn-sm btn-alt-danger"
                                    data-action="destroy"
                                    data-destroy-url="{{ route('admin.bids.destroy', $bid) }}"
                            >
                                <i class="far fa-fw fa-trash-alt"></i>
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Form data</h3>
                            </div>
                            <div class="block-content">
                                @foreach($bid->body as $key => $value)
                                    {{ Form::groupText('body['.$key.']', $key, $value, ['readonly']) }}
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">System data</h3>
                            </div>
                            <div class="block-content">
                                {{ Form::groupSelect('status', 'Status', \App\Models\Bid::STATUSES) }}
                                {{ Form::groupText('created_at', 'Created at', null, ['readonly']) }}
                                {{ Form::groupText('name', 'Form name', null, ['readonly']) }}
                                {{ Form::groupText('type', 'Type', null, ['readonly']) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection