@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Forums management</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Forums</li>
                    <li class="breadcrumb-item">List</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <a href="{{ route('admin.forum.create') }}" class="btn btn-primary">
            <i class="fa fa-fw fa-plus mr-1"></i>
            Add article
        </a>
    </div>
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Forums</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Title</th>
                            <th>slug</th>
                            <th>Comments</th>
                            <th style="width: 200px;">Published</th>
                            <th style="width: 200px;">Published Date</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($forums as $forum)
                            <tr>
                                <th class="text-center">{{ $forum->id }}</th>
                                <td class="font-w600">{{ $forum->title }}</td>
                                <td>{{ $forum->slug }}</td>
                                <td>{{ $forum->comments_count }}</td>
                                <td>{!! $forum->getIsPublishedBadge() !!}</td>
                                <td>{{ $forum->published_date }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.forum.edit', $forum) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                        <a href="{{ route('admin.comments.index', ['id' => $forum->id, 'type' => 'forum']) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Comments">
                                            <i class="far fa-comment-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $forums->links() }}
            </div>
        </div>
    </div>
@endsection
