<div class="row">
    <div class="col-sm-8">
        <div class="block">
            <div class="block-content">
                {{ Form::groupText('title', 'Title') }}
                {{ Form::groupTextarea('excerpt', 'Excerpt', null, ['rows' => 3]) }}
                {{ Form::groupTextarea('content', 'Content', null, ['id' => 'ckeditor-full']) }}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="block">
            <div class="block-content">
                {{ Form::groupSwitch('is_published', 'Published', '1', null, ['id' => 'is_published', 'switch-size' => 'lg']) }}
                {{ Form::groupDate('published_at', 'Published date', null, ['data-date-format' => 'Y-m-d']) }}
                {{ Form::groupText('slug', 'Slug (URL)') }}
                {{ Form::groupPreview('preview', 'Preview', ['accept' => 'image/*', 'hint' => 'min-width=600px, min-height=470px']) }}
                {{ Form::groupText('views', 'Views Count', null, ['readonly' => 'readonly']) }}
            </div>
        </div>
    </div>
</div>
@push('styles')
    <link rel="stylesheet" href="{{ asset('admin/js/plugins/flatpickr/flatpickr.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('admin/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('admin/js/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
    <script>
        jQuery(function () {
            One.helpers([
                'flatpickr',
            ]);
        });
    </script>
@endpush