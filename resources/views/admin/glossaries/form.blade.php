<div class="block">
    <div class="block-content">
        {{ Form::groupText('title', 'Title') }}
        {{ Form::groupTextarea('content', 'Content', null, ['id' => 'ckeditor-simple']) }}
    </div>
</div>

@push('scripts')
    <script src="{{ asset('admin/js/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
@endpush