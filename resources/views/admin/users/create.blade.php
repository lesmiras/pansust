@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Users management
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.users.index') }}">Users</a>
                    </li>
                    <li class="breadcrumb-item">Create user</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::open(['route' => 'admin.users.store']) }}
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">User create form</h3>
                            <div class="block-options">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="fa fa-fw fa-plus mr-1"></i>
                                    Create
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-sm-3 py-md-5">
                                <div class="col-md-8">
                                    {{ Form::groupText('name', 'Full Name') }}
                                    {{ Form::groupText('email', 'E-mail') }}
                                    {{ Form::groupText('password', 'Password') }}
                                    {{ Form::groupText('password_confirmation', 'Confirm Password') }}
                                    {{ Form::groupSelect('roles[]', 'Role', $roles, null, ['placeholder' => 'Select role...']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
