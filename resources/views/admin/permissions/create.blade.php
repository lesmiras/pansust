@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Permissions management
                <small class="d-block d-sm-inline-block font-size-base font-w400 text-muted">
                    New permission
                </small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.permissions.index') }}">Permissions</a>
                    </li>
                    <li class="breadcrumb-item">Create permission</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::open(['route' => 'admin.permissions.store']) }}
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Form</h3>
                        <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-fw fa-plus mr-1"></i>
                                Create
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row justify-content-center py-sm-3 py-md-5">
                            <div class="col-md-8">
                                {{ Form::groupText('name', 'Name') }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
