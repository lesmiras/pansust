@extends('admin.layouts.main')

@section('content')
    <!-- Hero Title -->
    <div class="bg-image overflow-hidden" style="background-image: url('/admin/media/photos/photo3@2x.jpg');">
        <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-center text-sm-left">
                    <div class="flex-sm-fill">
                        <h1 class="font-w600 text-white mb-0">Dashboard</h1>
                        <h4 class="font-w400 text-white-75 mb-0">Welcome Administrator</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content content-narrow">
        <!-- Stats -->
        <div class="row">
            <div class="col-4">
                <div class="block block-rounded block-link-pop border-left border-primary border-4x">
                    <div class="block-content block-content-full">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">SAT results</div>
                        <div class="font-size-h2 font-w400 text-dark">{{ $surveyTotal }}</div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="block block-rounded block-link-pop border-left border-primary border-4x">
                    <div class="block-content block-content-full">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Request</div>
                        <div class="font-size-h2 font-w400 text-dark">{{ $bidsTotal }}</div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="block block-rounded block-link-pop border-left border-primary border-4x">
                    <div class="block-content block-content-full">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Comments</div>
                        <div class="font-size-h2 font-w400 text-dark">{{ $commentsTotal }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Last SAT results</h3>
                <div class="block-options">
                    <a href="{{ route('admin.survey.results.index') }}" class="btn btn-sm btn-outline-secondary">Open all</a>
                </div>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Affiliation</th>
                        <th>Total Score</th>
                        <th style="width: 200px;">Created</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($surveys as $survey)
                        <tr>
                            <th class="text-center">{{ $survey->id }}</th>
                            <td>{{ $survey->name }}</td>
                            <td>{{ $survey->email }}</td>
                            <td>{{ $survey->affiliation }}</td>
                            <td>{{ $survey->total_score }}</td>
                            <td>{{ $survey->created_at }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('admin.survey.results.show', $survey) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Show">
                                        <i class="far fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Last request</h3>
                        <div class="block-options">
                            <a href="{{ route('admin.bids.index') }}" class="btn btn-sm btn-outline-secondary">Open all</a>
                        </div>
                    </div>
                    <div class="block-content">
                        <table class="table table-bordered table-vcenter table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th>Content</th>
                                    <th>Status</th>
                                    <th style="width: 200px;">Created at</th>
                                    <th class="text-center" style="width: 100px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($bids as $bid)
                                <tr>
                                    <th class="text-center">{{ $bid->id }}</th>
                                    <td>
                                        {!! implode('<br>', $bid->body) !!}
                                    </td>
                                    <td>{!! $bid->getStatusBadge() !!}</td>
                                    <td>{{ $bid->created_at }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.bids.edit', $bid) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-fw fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Last Comments</h3>
                        <div class="block-options">
                            <a href="{{ route('admin.comments.index') }}" class="btn btn-sm btn-outline-secondary">Open all</a>
                        </div>
                    </div>
                    <div class="block-content">
                        <table class="table table-bordered table-vcenter table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th>Data</th>
                                    <th>Status</th>
                                    <th style="width: 200px;">Created at</th>
                                    <th class="text-center" style="width: 100px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $comment)
                                <tr>
                                    <th class="text-center">{{ $comment->id }}</th>
                                    <td>
                                        <span>{{ $comment->name }}</span><br>
                                        <span>{{ $comment->email }}</span><br>
                                        <span>{{ Str::limit(strip_tags($comment->content), 50, '...') }}</span>
                                    </td>
                                    <td>{!! $comment->getStatusBadge() !!}</td>
                                    <td>{{ $comment->created_at }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.comments.edit', $comment) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-fw fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
