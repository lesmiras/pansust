<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Pansust — Панель администратора</title>
    <meta name="description" content="Pansust — Панель администратора">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('admin/media/favicons/favicon.png') }}">
    <!-- Fonts and Styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('admin/css/oneui.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">
    @stack('styles')
    <!-- Scripts -->
    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
</head>
<body>
    <div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark side-scroll page-header-fixed">
        <!-- Sidebar -->
        <nav id="sidebar" aria-label="Main Navigation">
            <!-- Side Header -->
            <div class="content-header bg-white-5">
                <!-- Logo -->
                <a class="font-w600 text-dual" href="{{ route('admin.home') }}">
                    <i class="fa fa-circle-notch text-primary"></i>
                    <span class="smini-hide">
                        <span class="font-w700 font-size-h5">PANSUST</span>
                    </span>
                </a>
                <!-- Options -->
                <div>
                    <!-- Close Sidebar -->
                    <a class="d-lg-none text-dual ml-3" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <!-- Side Navigation -->
            <div class="content-side content-side-full">
                <ul class="nav-main">
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->routeIs('admin.dashboard') ? ' active' : '' }}" href="{{ route('admin.home') }}">
                            <i class="nav-main-link-icon si si-speedometer"></i>
                            <span class="nav-main-link-name">Dashboard</span>
                        </a>
                    </li>
                    @can('survey-manage')
                        <li class="nav-main-item{{ request()->routeIs('admin.survey.*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-energy"></i>
                                <span class="nav-main-link-name">Survey</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a href="{{ route('admin.survey.results.index') }}"
                                       class="nav-main-link{{ request()->routeIs('admin.survey.results.*') ? ' active' : '' }}"
                                    >
                                        <span class="nav-main-link-name">Results</span>
                                    </a>
                                    <a href="{{ route('admin.survey.factors.index') }}"
                                       class="nav-main-link{{ request()->routeIs('admin.survey.factors.*') ? ' active' : '' }}"
                                    >
                                        <span class="nav-main-link-name">Factors</span>
                                    </a>
                                    <a href="{{ route('admin.survey.indicators.index') }}"
                                       class="nav-main-link{{ request()->routeIs('admin.survey.indicators.*') ? ' active' : '' }}"
                                    >
                                        <span class="nav-main-link-name">Indicators</span>
                                    </a>
                                    <a href="{{ route('admin.survey.sub-indicators.index') }}"
                                       class="nav-main-link{{ request()->routeIs('admin.survey.sub-indicators.*') ? ' active' : '' }}"
                                    >
                                        <span class="nav-main-link-name">Sub-Indicators</span>
                                    </a>
                                    <a href="{{ route('admin.survey.questions.index') }}"
                                       class="nav-main-link{{ request()->routeIs('admin.survey.questions.*') ? ' active' : '' }}"
                                    >
                                        <span class="nav-main-link-name">Questions</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endcan
                    @can('news-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.news.*') ? ' active' : '' }}" href="{{ route('admin.news.index') }}">
                                <i class="nav-main-link-icon far fa-newspaper"></i>
                                <span class="nav-main-link-name">News</span>
                            </a>
                        </li>
                    @endcan
                    @can('events-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.events.*') ? ' active' : '' }}" href="{{ route('admin.events.index') }}">
                                <i class="nav-main-link-icon far fa-newspaper"></i>
                                <span class="nav-main-link-name">Events</span>
                            </a>
                        </li>
                    @endcan
                    @can('forums-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.forum.*') ? ' active' : '' }}" href="{{ route('admin.forum.index') }}">
                                <i class="nav-main-link-icon far fa-newspaper"></i>
                                <span class="nav-main-link-name">Forum</span>
                            </a>
                        </li>
                    @endcan
                    @can('comments-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.comments.*') ? ' active' : '' }}" href="{{ route('admin.comments.index') }}">
                                <i class="nav-main-link-icon far fa-comment-alt"></i>
                                <span class="nav-main-link-name">Comments</span>
                            </a>
                        </li>
                    @endcan
                    @can('glossaries-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.glossaries.*') ? ' active' : '' }}" href="{{ route('admin.glossaries.index') }}">
                                <i class="nav-main-link-icon fab fa-wikipedia-w"></i>
                                <span class="nav-main-link-name">Glossary</span>
                            </a>
                        </li>
                    @endcan
                    @can('bids-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.bids.*') ? ' active' : '' }}" href="{{ route('admin.bids.index') }}">
                                <i class="nav-main-link-icon fa fa-phone-alt"></i>
                                <span class="nav-main-link-name">Requests</span>
                            </a>
                        </li>
                    @endcan
                    <li class="nav-main-item">
                        <a class="nav-main-link{{ request()->routeIs('admin.fm.index') ? ' active' : '' }}" href="{{ route('admin.fm.index') }}">
                            <i class="nav-main-link-icon far fa-file-image"></i>
                            <span class="nav-main-link-name">File-manager</span>
                        </a>
                    </li>
                    @can('settings-manage')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.settings.*') ? ' active' : '' }}" href="{{ route('admin.settings.index') }}">
                                <i class="nav-main-link-icon si si-settings"></i>
                                <span class="nav-main-link-name">Settings</span>
                            </a>
                        </li>
                    @endcan
                    @canany(['users-manage', 'roles-manage', 'permissions-manage'])
                        <li class="nav-main-item {{ request()->routeIs('admin.users.*', 'admin.roles.*', 'admin.permissions.*') ? 'open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="nav-main-link-icon fa fa-users-cog"></i>
                                <span class="nav-main-link-name">Users management</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @can('users-manage')
                                    <li class="nav-main-item">
                                        <a class="nav-main-link {{ request()->routeIs('admin.users.*') ? 'active' : '' }}" href="{{ route('admin.users.index') }}">
                                            <i class="nav-main-link-icon fa fa-users"></i>
                                            <span class="nav-main-link-name">Users</span>
                                        </a>
                                    </li>
                                @endcan
                                @can('roles-manage')
                                    <li class="nav-main-item">
                                        <a class="nav-main-link {{ request()->routeIs('admin.roles.*') ? 'active' : '' }}" href="{{ route('admin.roles.index') }}">
                                            <i class="nav-main-link-icon fa fa-user-lock"></i>
                                            <span class="nav-main-link-name">Roles</span>
                                        </a>
                                    </li>
                                @endcan
                                @can('permissions-manage')
                                    <li class="nav-main-item">
                                        <a class="nav-main-link {{ request()->routeIs('admin.permissions.*') ? 'active' : '' }}" href="{{ route('admin.permissions.index') }}">
                                            <i class="nav-main-link-icon fa fa-user-shield"></i>
                                            <span class="nav-main-link-name">Permissions</span>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                </ul>
            </div>
        </nav>
        <!-- Header -->
        <header id="page-header">
            <!-- Header Content -->
            <div class="content-header">
                <!-- Left Section -->
                <div class="d-flex align-items-center">
                    <!-- Toggle Sidebar -->
                    <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>
                    <!-- Toggle Mini Sidebar -->
                    <button type="button" class="btn btn-sm btn-dual mr-2 d-none d-lg-inline-block" data-toggle="layout" data-action="sidebar_mini_toggle">
                        <i class="fa fa-fw fa-ellipsis-v"></i>
                    </button>
                </div>
                <!-- Right Section -->
                <div class="d-flex align-items-center">
                    <!-- User Dropdown -->
                    <div class="dropdown d-inline-block ml-2">
                        <button type="button" class="btn btn-sm btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="rounded" src="{{ asset('admin/media/avatars/avatar.jpg') }}" style="width: 18px;">
                            <span class="d-none d-sm-inline-block ml-1">{{ auth()->user()->name }}</span>
                            <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="page-header-user-dropdown">
                            <div class="p-2">
                                <h5 class="dropdown-header text-uppercase">User Options</h5>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                                    <span>Profile</span>
                                    <i class="si si-user ml-1"></i>
                                </a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                                    <span>Settings</span>
                                    <i class="si si-settings"></i>
                                </a>
                                <div role="separator" class="dropdown-divider"></div>
                                <h5 class="dropdown-header text-uppercase">Actions</h5>
                                <form action="{{ route('admin.logout') }}" method="POST">
                                    @csrf
                                    <button class="dropdown-item d-flex align-items-center justify-content-between">
                                        <span>Log Out</span>
                                        <i class="si si-logout ml-1"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Main Container -->
        <main id="main-container">
            @if(session('success'))
                <div class="alert alert-success mb-0" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                    <p class="mb-0">{{ session('success') }}</p>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger mb-0" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                    An Error Occurred :(
                    {{--<ul class="list-unstyled mb-0 mt-1">
                        {!! implode('', $errors->all('<li><i class="fa fa-info-circle"></i> :message</li>')) !!}
                    </ul>--}}
                </div>
            @endif
            @yield('content')
        </main>
        <!-- Footer -->
        <footer id="page-footer" class="bg-body-light">
            <div class="content py-3">
            </div>
        </footer>
    </div>
    <!-- Modals -->
    <div class="modal" id="modal-destroy" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Внимание!</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Вы уверены что хотите удалить?</p>
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light text-right">
                        <button class="btn btn-alt-secondary" data-dismiss="modal">Нет</button>
                        <button class="btn btn-alt-success" data-action="confirm-destroy">Да</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ mix('admin/js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
