@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Comments management
                <small class="d-block d-sm-inline-block font-size-base font-w400 text-muted">
                    {{ $comment->title }}
                </small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.comments.index') }}">Comments</a>
                    </li>
                    <li class="breadcrumb-item">Edit</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        {{ Form::model($comment, ['route' => ['admin.comments.update', $comment->id], 'method' => 'put', 'files' => true]) }}
        <div class="block">
            <div class="block-header">
                <div class="block-options">
                    <a href="{{ route('admin.comments.index') }}" class="btn btn-sm btn-outline-secondary">
                        <i class="fa fa-fw fa-angle-left"></i>
                        Back to list
                    </a>
                </div>
                <div class="block-options">
                    <button type="submit" class="btn btn-sm btn-primary btn-save">
                        <i class="fa fa-fw fa-save"></i>
                        Save
                    </button>
                    <button type="button"
                            class="btn btn-sm btn-alt-danger"
                            data-action="destroy"
                            data-destroy-url="{{ route('admin.comments.destroy', $comment) }}"
                    >
                        <i class="far fa-fw fa-trash-alt"></i>
                        Delete
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="block">
                    <div class="block-content">
                        {{ Form::groupText('name', 'Name') }}
                        {{ Form::groupText('email', 'E-mail') }}
                        {{ Form::groupText('post', 'Post') }}
                        {{ Form::groupTextarea('content', 'Content') }}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block">
                    <div class="block-content">
                        {{ Form::groupSelect('status', 'Status', \App\Models\Comment::STATUSES) }}
                        {{ Form::groupText('created_at', 'Created at', null, ['readonly' => 'readonly']) }}
                    </div>
                </div>
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Related article</h3>
                    </div>
                    <div class="block-content">
                        <p>
                            <a href="{{ $comment->commentable->getEditLink() }}">{{ $comment->commentable->getCommentableTitle() }}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection