@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Comments management</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Comments</li>
                    <li class="breadcrumb-item">List</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        @if($commentable)
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Comments for</h3>
                </div>
                <div class="block-content">
                    <p>
                        <b>{{ $commentable->getCommentableType() }}:</b>
                        <a href="{{ $commentable->getEditLink() }}">{{ $commentable->getCommentableTitle() }}</a>
                    </p>
                </div>
            </div>
        @endif
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Comments</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Article</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Content</th>
                            <th>Status</th>
                            <th style="width: 200px;">Created at</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comments as $comment)
                            <tr>
                                <th class="text-center">{{ $comment->id }}</th>
                                <td class="font-w600">
                                    <a href="{{ $comment->commentable->getEditLink() }}">
                                        {{ $comment->commentable->getCommentableTitle() }}
                                    </a>
                                </td>
                                <td>{{ $comment->name }}</td>
                                <td>{{ $comment->email }}</td>
                                <td>{{ Str::limit(strip_tags($comment->content), 50, '...') }}</td>
                                <td>{!! $comment->getStatusBadge() !!}</td>
                                <td>{{ $comment->created_at }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.comments.edit', $comment) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $comments->links() }}
            </div>
        </div>
    </div>
@endsection
