@extends('admin.layouts.main')

@section('content')
    <div style="height: calc(100vh - 100px);">
        <div id="fm"></div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
@endpush
