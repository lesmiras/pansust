@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Survey Sub-Indicators management</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Sub-Indicators</li>
                    <li class="breadcrumb-item">List</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <a href="{{ route('admin.survey.sub-indicators.create') }}" class="btn btn-primary">
            <i class="fa fa-fw fa-plus mr-1"></i>
            Add sub-indicator
        </a>
    </div>
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Sub-Indicators</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Name</th>
                            <th>Weight</th>
                            <th>Indicator</th>
                            <th style="width: 200px;">Created</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subIndicators as $subIndicator)
                            <tr>
                                <th class="text-center">{{ $subIndicator->id }}</th>
                                <td class="font-w600">{{ $subIndicator->name }}</td>
                                <td>{{ $subIndicator->weight }}</td>
                                <td>{{ $subIndicator->indicator->name }}</td>
                                <td>{{ $subIndicator->created_at }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.survey.sub-indicators.edit', $subIndicator) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
