<div class="block">
    <div class="block-content">
        {{ Form::groupSelect('indicator_id', 'Indicator', $indicators, null, ['placeholder' => 'Select indicator...']) }}
        {{ Form::groupText('name', 'Name') }}
        {{ Form::groupText('weight', 'Weight') }}
    </div>
</div>
