@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Survey Indicators management</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.survey.indicators.index') }}">Questions</a>
                    </li>
                    <li class="breadcrumb-item">Create</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::open(['route' => 'admin.survey.indicators.store', 'files' => true]) }}
                <div class="block">
                    <div class="block-header block-header-default">
                        <div class="block-options">
                            <a href="{{ route('admin.survey.indicators.index') }}" class="btn btn-sm btn-outline-secondary">
                                <i class="fa fa-fw fa-angle-left"></i>
                                Back to list
                            </a>
                        </div>
                        <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-primary btn-save">
                                <i class="fa fa-fw fa-save"></i>
                                Create
                            </button>
                        </div>
                    </div>
                </div>
                @include('admin.survey.indicators.form')
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection