<div class="block">
    <div class="block-content">
        {{ Form::groupSelect('factor_id', 'Factor', $factors, null, ['placeholder' => 'Select factor...']) }}
        {{ Form::groupText('name', 'Name') }}
        {{ Form::groupText('weight', 'Weight') }}
    </div>
</div>
