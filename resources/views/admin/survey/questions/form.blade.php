<div class="block">
    <div class="block-header">
        <h3 class="block-title">Question Form</h3>
    </div>
    <div class="block-content">
        @php
            $indicatorOptions = [
                'placeholder' => 'Select...',
                'id' => 'question_indicator_id',
            ];

            $subIndicatorOptions = [
                'placeholder' => 'Select...',
                'id' => 'question_sub_indicator_id',
            ];

            if (empty($indicators)) {
                $indicatorOptions['disabled'] = 'disabled';
            }

            if (empty($subIndicators)) {
                $subIndicatorOptions['disabled'] = 'disabled';
            }
        @endphp

        {{ Form::groupSwitch('is_published', 'Published', '1', null, ['id' => 'is_published', 'switch-size' => 'lg']) }}
        {{ Form::groupSelect(
            'factor_id',
            'Factor',
            $factors,
            null,
            [
                'placeholder' => 'Select...',
                'id' => 'question_factor_id',
            ]
        ) }}
        {{ Form::groupSelect(
            'indicator_id',
            'Indicator',
            $indicators,
            null,
            $indicatorOptions
        ) }}
        {{ Form::groupSelect(
            'sub_indicator_id',
            'Sub-Indicator',
            $subIndicators,
            null,
            $subIndicatorOptions
        ) }}
        {{ Form::groupText('question', 'Question') }}
        {{ Form::groupText('multiplier', 'Multiplier') }}
        {{ Form::groupSelect('type', 'Type', \App\Models\SurveyQuestion::TYPES, null, ['placeholder' => 'Select...', 'id' => 'question_type']) }}
    </div>
</div>
<div class="block" id="answers-block">
    <div class="block-header">
        <h3 class="block-title">Answers Form</h3>
    </div>
    <div class="block-content">
        @error('answers')
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ $message }}</p>
            </div>
        @enderror

        <div class="answer-form-wrapper {{ $questionType === \App\Models\SurveyQuestion::TYPE_INPUT ? '' : 'hidden' }}" id="answer-form-wrapper">
            <div class="row">
                <div class="col">
                    <div class="form-group {{ $errors->has('answer.weight') ? 'is-invalid' : '' }}">
                        <label for="answer[weight]">Answer Weight</label>
                        <input type="text"
                               name="answer[weight]"
                               class="form-control"
                               value="{{ $answerData['weight'] ?? '' }}"
                        >
                        {!! $errors->first('answer.weight', '<span class="invalid-feedback">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('answer.type') ? 'is-invalid' : '' }}">
                        <label for="answer[type]">Answer Type</label>
                        <select name="answer[type]" class="form-control">
                            <option selected disabled>Select type ...</option>
                            <option value="number" {{ isset($answerData['type']) && $answerData['type'] === 'number' ? 'selected' : '' }}>Integer or float number</option>
                            <option value="integer" {{ isset($answerData['type']) && $answerData['type'] === 'integer' ? 'selected' : '' }}>Only Integer number</option>
                            <option value="float" {{ isset($answerData['type']) && $answerData['type'] === 'float' ? 'selected' : '' }}>Only Float number</option>
                        </select>
                        {!! $errors->first('answer.type', '<span class="invalid-feedback">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('answer.min_value') ? 'is-invalid' : '' }}">
                        <label for="answer[min_value]">Minimum value</label>
                        <input type="text"
                               name="answer[min_value]"
                               class="form-control"
                               value="{{ $answerData['min_value'] ?? '' }}"
                        >
                        {!! $errors->first('answer.min_value', '<span class="invalid-feedback">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('answer.max_value') ? 'is-invalid' : '' }}">
                        <label for="answer[max_value]">Maximum value</label>
                        <input type="text"
                               name="answer[max_value]"
                               class="form-control"
                               value="{{ $answerData['max_value'] ?? '' }}"
                        >
                        {!! $errors->first('answer.max_value', '<span class="invalid-feedback">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="answers-table-wrapper {{ \App\Models\SurveyQuestion::isSelectableType($questionType) ? '' : 'hidden' }}" id="answers-table-wrapper">
            <table class="table table-hover table-vcenter" id="answers-table">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th style="width: 100%;">Title</th>
                        <th class="text-center" style="min-width: 100px;">Weight</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="hidden answer-row" id="answer-row-template">
                        <input type="hidden" class="answer-id" value="0" data-name="id">
                        <th class="text-center">1</th>
                        <td class="font-w600 font-size-sm">
                            <input type="text" class="form-control" data-name="title">
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <input type="text" class="form-control" data-name="weight">
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button"
                                        class="btn btn-sm btn-light js-tooltip-enabled answer-row-remove"
                                        data-toggle="tooltip"
                                        title="Remove answer"
                                        data-original-title="Remove answer"
                                >
                                    <i class="fa fa-fw fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    @if(!empty($answerList))
                        @foreach($answerList as $key => $answer)
                            <tr class="answer-row" id="answer-row-{{ $answer['id'] }}">
                                <input type="hidden" class="answer-id" name="answers[{{ $answer['id'] }}][id]" value="{{ $answer['id'] }}">
                                <th class="text-center">{{ $answer['id'] }}</th>
                                <td class="font-w600 font-size-sm">
                                    <input type="text"
                                           class="form-control {{ $errors->has("answers.{$answer['id']}.title") ? 'is-invalid' : '' }}"
                                           name="answers[{{ $answer['id'] }}][title]"
                                           value="{{ $answer['title'] }}"
                                    >
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    <input type="text"
                                           class="form-control {{ $errors->has("answers.{$answer['id']}.weight") ? 'is-invalid' : '' }}"
                                           name="answers[{{ $answer['id'] }}][weight]"
                                           value="{{ $answer['weight'] }}"
                                    >
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-sm btn-light js-tooltip-enabled answer-row-remove"
                                                data-toggle="tooltip"
                                                title="Remove answer"
                                                data-original-title="Remove answer"
                                        >
                                            <i class="fa fa-fw fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <div class="text-right mb-3">
                <button type="button" class="btn btn-outline-secondary" id="add-answer">
                    <i class="fa fa-fw fa-plus"></i> Add Answer
                </button>
            </div>
        </div>
    </div>
</div>
