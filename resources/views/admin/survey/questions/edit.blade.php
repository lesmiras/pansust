@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Survey Questions management
                <small class="d-block d-sm-inline-block font-size-base font-w400 text-muted">
                    {{ $question->question }}
                </small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.survey.questions.index') }}">Questions</a>
                    </li>
                    <li class="breadcrumb-item">Edit</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::model($question, ['route' => ['admin.survey.questions.update', $question->id], 'method' => 'put']) }}
                <div class="block">
                    <div class="block-header">
                        <div class="block-options">
                            <a href="{{ route('admin.survey.questions.index') }}" class="btn btn-sm btn-outline-secondary">
                                <i class="fa fa-fw fa-angle-left"></i>
                                Back to list
                            </a>
                        </div>
                        <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-primary btn-save">
                                <i class="fa fa-fw fa-save"></i>
                                Save
                            </button>
                            <button type="button"
                                    class="btn btn-sm btn-alt-danger"
                                    data-action="destroy"
                                    data-destroy-url="{{ route('admin.survey.questions.destroy', $question) }}"
                            >
                                <i class="far fa-fw fa-trash-alt"></i>
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
                @include('admin.survey.questions.form')
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection