@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Survey Results</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Results</li>
                    <li class="breadcrumb-item">List</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Results</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Affiliation</th>
                        <th>Total Score</th>
                        <th style="width: 200px;">Created</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <th class="text-center">{{ $result->id }}</th>
                            <td>{{ $result->name }}</td>
                            <td>{{ $result->email }}</td>
                            <td>{{ $result->affiliation }}</td>
                            <td>{{ $result->total_score }}</td>
                            <td>{{ $result->created_at }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('admin.survey.results.show', $result) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Show">
                                        <i class="far fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $results->links() }}
            </div>
        </div>
    </div>
@endsection
