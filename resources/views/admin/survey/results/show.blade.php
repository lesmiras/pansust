@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Survey Result
                <small class="d-block d-sm-inline-block font-size-base font-w400 text-muted">
                    {{ $result->uuid }}
                </small>
            </h1>
            <a href="{{ route('survey.showResult', $result->uuid) }}" class="btn btn-outline-secondary" target="_blank">Open in public</a>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.survey.results.index') }}">Results</a>
                    </li>
                    <li class="breadcrumb-item">Show</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <div class="block">
            <div class="block-content">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 100%;">
                                <span class="font-size-h3">Total score</span>
                            </td>
                            <td><span class="badge badge-primary font-size-h3">{{ $result->total_score }}</span></td>
                        </tr>
                        <tr>
                            <td><span class="font-size-h5">Total ENV</span></td>
                            <td><span class="badge badge-secondary font-size-h5">{{ $result->total_factors['env'] }}</span></td>
                        </tr>
                        <tr>
                            <td><span class="font-size-h5">Total ECO</span></td>
                            <td><span class="badge badge-secondary font-size-h5">{{ $result->total_factors['eco'] }}</span></td>
                        </tr>
                        <tr>
                            <td><span class="font-size-h5">Total S&F</span></td>
                            <td><span class="badge badge-secondary font-size-h5">{{ $result->total_factors['sf'] }}</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Form data</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Created at</label>
                            <input type="text" class="form-control" value="{{ $result->created_at }}" readonly>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" value="{{ $result->name }}" readonly>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" class="form-control" value="{{ $result->email }}" readonly>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Affiliation</label>
                            <input type="text" class="form-control" value="{{ $result->affiliation }}" readonly>
                        </div>
                    </div>
                </div>
                @if($result->additional)
                    <div class="row">
                        @foreach($result->additional as $additionalKey => $additionalValue)
                            <div class="col">
                                <div class="form-group">
                                    <label>{{ $additionalKey }}</label>
                                    <input type="text" class="form-control" value="{{ $additionalValue }}" readonly>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">User answers</h3>
            </div>
            <div class="block-content">
                @foreach($factors as $factor)
                    <table class="table table-borderless">
                        <thead class="border-bottom">
                            <tr>
                                <th colspan="3" style="width: 100%">
                                    <h3 class="mb-0">{{ $factor['name'] }}</h3>
                                </th>
                                <th class="text-center" style="min-width: 100px;">
                                    <h3 class="mb-0">
                                        <span class="badge badge-info">{{ $factor['score'] }}</span>
                                    </h3>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($factor['indicators'] as $indicator)
                                <tr class="table-info">
                                    <td colspan="3">
                                        <b>{{ $indicator['name'] }}</b>
                                    </td>
                                    <th class="text-center" style="min-width: 100px;">
                                        {{ $indicator['score'] }}
                                    </th>
                                </tr>

                                @if($indicator['sub_indicators'])
                                    @foreach($indicator['sub_indicators'] as $subIndicator)
                                        <tr class="table-active">
                                            <td colspan="3">{{ $subIndicator['name'] }}</td>
                                            <th class="text-center" style="min-width: 100px;">
                                                {{ $subIndicator['score'] }}
                                            </th>
                                        </tr>

                                        @if($subIndicator['questions'])
                                            <tr class="border-bottom font-size-sm">
                                                <th>#</th>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th class="text-center">Weight</th>
                                            </tr>
                                            @foreach($subIndicator['questions'] as $question)
                                                <tr class="border-bottom">
                                                    <td class="text-center">
                                                        {{ $loop->iteration }}
                                                    </td>
                                                    <td style="width: 100%">
                                                        {{ $question['question']['question'] }}
                                                    </td>
                                                    <td style="min-width: 300px;">
                                                        @if(isset($question['answer']['items']))
                                                            {!! implode('<br>', array_column($question['answer']['items'], 'title')) !!}
                                                        @else
                                                            {{ $question['answer']['title'] }}
                                                        @endif
                                                    </td>
                                                    <td class="text-center" style="min-width: 100px;">
                                                        {{ $question['answer']['weight'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                @endforeach
            </div>
        </div>
    </div>
@endsection