@extends('admin.layouts.main')

@section('content')
    <div class="content content-full bg-body-light">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">News management</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">News</li>
                    <li class="breadcrumb-item">List</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="content">
        <a href="{{ route('admin.news.create') }}" class="btn btn-primary">
            <i class="fa fa-fw fa-plus mr-1"></i>
            Add news
        </a>
    </div>
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">News</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Title</th>
                            <th>slug</th>
                            <th>Comments</th>
                            <th style="width: 200px;">Published</th>
                            <th style="width: 200px;">Published Date</th>
                            <th class="text-center" style="min-width: 100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($news as $newsItem)
                            <tr>
                                <th class="text-center">{{ $newsItem->id }}</th>
                                <td class="font-w600">{{ $newsItem->title }}</td>
                                <td>{{ $newsItem->slug }}</td>
                                <td>{{ $newsItem->comments_count }}</td>
                                <td>{!! $newsItem->getIsPublishedBadge() !!}</td>
                                <td>{{ $newsItem->published_date }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.news.edit', $newsItem) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                        <a href="{{ route('admin.comments.index', ['id' => $newsItem->id, 'type' => 'news']) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Comments">
                                            <i class="far fa-comment-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $news->links() }}
            </div>
        </div>
    </div>
@endsection
