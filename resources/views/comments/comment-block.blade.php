<div class="comment-block">
    <div class="comment-block-inner">
        <div class="comment-block__header">
            <div class="comment-block__header-icon">
                <img src="{{ asset('assets/images/icon_author.svg') }}" alt="#">
            </div>
            <div class="comment-block__header-info">
                <div class="comment-block__author">{{ $comment->name }}</div>
                <div class="comment-block__published">Wrote {{ $comment->createdAtFormatted() }}</div>
            </div>
        </div>
        <div class="comment-block__content">
            <div class="comment-block__text">
                {{ $comment->content }}
            </div>
        </div>
    </div>
</div>