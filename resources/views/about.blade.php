@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">About us</h1>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="column-md-7 page-content-text">
                        <h3>PANSUST</h3>
                        <p>
                            is a multidisciplinary project aiming for the development of building sustainability
                            features and assessment tools for Kazakhstan, which has a vibrant and diverse culture
                            influenced by many ethnic groups.
                        </p>
                        <h3>THE PROJECT</h3>
                        <p>
                            creates a Think Tank and brings together all possible stakeholders to present their ideas on
                            how to contribute to sustainable building development
                            All stakeholders including civil engineers, urbanists, designers, architects, health service
                            providers, university students, artists, civil activists, and associates will be asked to
                            engage and present their ideas on how to adapt residential building sustainability factors
                            and indicators to future pandemic conditions.
                        </p>
                        <h3>BY USING</h3>
                        <p>
                            the knowledge developed in the Think Tank, we will then look for better solutions to:
                        </p>
                        <ol>
                            <li>
                                maintain the sustainability performance of buildings under pandemic measures
                            </li>
                            <li>
                                develop a novel building sustainability assessment (rating) tool which adequately
                                addresses a building’s sustainability performance under social isolation times
                            </li>
                            <li>
                                ensure these changes support and enhance residents’ well-being during the events
                            </li>
                        </ol>
                    </div>
                    <div class="column-md-5 page-column-image">
                        <img src="{{ asset('assets/images/upload/about-image.jpg') }}" class="img-fluid"/>
                    </div>
                </div>
            </div>
            <section class="section section-areas">
                <div class="section-title-wrapper">
                    <h2 class="section-title">Areas of focus</h2>
                </div>
                <div class="areas-list">
                    <div class="areas-list__item">
                        <div class="areas-list__item-image">
                            <img src="{{ asset('assets/images/upload/about-block-image-1.jpg') }}" alt="#">
                        </div>
                        <div class="areas-list__item-content">
                            <div class="areas-list__item-inner">
                                <h3 class="areas-list__item-title">Residential buildings</h3>
                                <div class="areas-list__item-text">
                                    The COVID-19 pandemic is bringing about changes, and alongside these, we can alter
                                    the way we design our living spaces. Residential buildings and living spaces usually
                                    been designed with concepts promoting comfort levels for daily routines. This will
                                    not be sufficient anymore in our cities where people may need to stay indoors for
                                    months/years in isolation due to the need to keep a social distance from the other
                                    members of society as a part of pandemic measures. Consequently, our homes become an
                                    essential part of more complex future living environments, and designing
                                    multifunctional spaces would be the new norm in a sustainable building paradigm.
                                    That is, health and safety, sustainable use of environmental resources, and comfort
                                    for the occupants are important criteria that residential buildings should have to
                                    fulfill sustainability requirements. Residential houses should provide certain
                                    health and safety protective measures to their occupants, such as the application of
                                    new touchless technologies, having proper sanitation to diminish the probability of
                                    getting infected, and developing greener and more intimate spaces that can help
                                    recover and improve mental states.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="areas-list__item">
                        <div class="areas-list__item-image">
                            <img src="{{ asset('assets/images/upload/about-block-image-2.jpg') }}" alt="#">
                        </div>
                        <div class="areas-list__item-content">
                            <div class="areas-list__item-inner">
                                <h3 class="areas-list__item-title">Sustainability</h3>
                                <div class="areas-list__item-text">
                                    As staying at home for more extended periods of lockdowns becomes a new lifestyle
                                    for many people, it is leading to more extensive consumption of the resources in
                                    homes such as energy and water and changes in waste generation mechanisms and
                                    profiles at the household level. It also creates an anthropogenic impact on air,
                                    water, and soil systems. As a response to environmental needs, sustainable
                                    technologies mainly and particularly need to address the issue of improving the
                                    increased consumption of energy and water.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="areas-list__item">
                        <div class="areas-list__item-image">
                            <img src="{{ asset('assets/images/upload/about-block-image-3.jpg') }}" alt="#">
                        </div>
                        <div class="areas-list__item-content">
                            <div class="areas-list__item-inner">
                                <h3 class="areas-list__item-title">Green building assessment systems</h3>
                                <div class="areas-list__item-text">
                                    One of the consequences of COVID-19 pandemic is the momentum it has created for
                                    global changes affecting various aspects of daily lives. Among these, green building
                                    certification systems (GBCSs) should not be left behind as significant potential
                                    modifications may be required to ensure their versatility for residential buildings
                                    due to the new pandemic reality. PANSUST believes that green buildings assessment
                                    systems need to reconsider their criteria to provide sustainability during different
                                    periods of time, including pandemics, thus, proposing a particular set of
                                    sustainability indicators covering special sustainability requirements under
                                    pandemic conditions. We specifically expect that existing building sustainability
                                    rating methods that mainly favor “environmental impact” and “energy performance” of
                                    buildings will receive a significant shift towards emphasizing “social and health”
                                    aspects.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section section-subscribe">
                <div class="section-inner">
                    <h3 class="section-title">
                        Do you want to become a member of PANSUST and <a href="#" data-toggle="modal" data-target="#modal-subscribe">subscribe</a> to our news?
                    </h3>
                </div>
            </section>
        </div>
    </div>
@endsection
