@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">Pansust SAT</h1>
            </div>
            <div class="sat-result-wrapper">
                <div class="sat-result__header">
                    <div class="sat-result__icon"></div>
                    <div class="sat-result__title">Thank you for completing our questionnaire!</div>
                    <div class="sat-result__text">
                        Basing on rapid sustainability assessment method, the overall calculated score
                        for the building is: {{ $result->total_score }}
                    </div>
                </div>
                <div class="sat-result__graph">
                    <canvas id="sat-graph"
                            data-total-env="{{ $result->total_factors['env'] }}"
                            data-total-eco="{{ $result->total_factors['eco'] }}"
                            data-total-sf="{{ $result->total_factors['sf'] }}"
                    ></canvas>
                </div>
                <div class="sat-result__footer">
                    <div class="share-wrapper">
                        <div class="share-title">SHARE THIS</div>
                        @include('partials.share-list')
                    </div>
                    <a href="{{ route('survey.index') }}" class="button button-primary">Go back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
