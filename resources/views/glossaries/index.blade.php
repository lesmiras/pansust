@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper page-title-no-mb">
                <h1 class="page-title">Pansust wikipedia</h1>
            </div>
            <div class="page-header-search">
                <form action="{{ route('glossaries.index') }}" class="page-search__form">
                    <div class="page-search__icon"></div>
                    <input type="text" class="input page-search__input" placeholder="SEARCH IN PANSUST WIKIPEDIA" name="search" value="{{ request()->get('search') }}">
                </form>
            </div>
            <div class="wiki-letters-wrapper">
                <ul class="wiki-letters">
                    @foreach($letters as $letter)
                        <li class="wiki-letters__item {{ request()->is('wiki/' . $letter) ? 'wiki-letters__item--active' : '' }}">
                            <a href="{{ route('glossaries.show', $letter) }}" class="wiki-letters__link">
                                {{ $letter }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @if(count($glossaries))
                <div class="wiki-list-wrapper">
                    <div class="wiki-list">
                        @foreach($glossaries as $glossary)
                            <div class="wiki-list__item">
                                <div class="wiki-list__item-header">
                                    <h4 class="wiki-list__item-title">{{ $glossary->title }}</h4>
                                </div>
                                <div class="wiki-list__item-body">
                                    <div class="wiki-list__item-text">
                                        {!! $glossary->content !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @elseif($inSearch && !count($glossaries))
                <div class="search-empty-wrapper">
                    <div class="search-empty-inner">
                        <div class="search-empty__title">No Results To Show</div>
                    </div>
                </div>
            @endif
            <section class="section section-subscribe">
                <div class="section-inner">
                    <h3 class="section-title">
                        Do you want to become a member of PANSUST and <a href="#" data-toggle="modal" data-target="#modal-subscribe">subscribe</a> to our news?
                    </h3>
                </div>
            </section>
        </div>
    </div>
@endsection
