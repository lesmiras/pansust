@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-title-wrapper page-title-linked">
                <h1 class="page-title">Events</h1>
                @if(request()->get('order') === 'popular')
                    <a href="{{ route('events.index') }}" class="page-title-link button-sort">Latest events</a>
                @else
                    <a href="{{ route('events.index', ['order' => 'popular']) }}" class="page-title-link button-sort">Popular events</a>
                @endif
            </div>
            <div class="article-blocks">
                @foreach($events as $event)
                    @include('events.partial.article-block', ['article' => $event])
                @endforeach
            </div>
            @if($events->lastPage() > 1)
                <div class="page-load-more">
                    <button class="button button-primary"
                            data-load-articles="events"
                            data-page="1"
                            data-order="{{ request()->get('order') }}"
                    >
                        Load more
                    </button>
                </div>
            @endif
        </div>
    </div>
@endsection
