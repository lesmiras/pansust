// Import global dependencies
import './bootstrap';

// Import required modules
import Template from './oneui/template';
import './pages/survey';

// App extends Template
export default class App extends Template {
    constructor() {
        super();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    _uiInit() {
        // Call original function
        super._uiInit();

        const th = this;

        this._initDestroyModal();
        this._initCkeditor();

        // slug generator
        $('input[name="slug"]').on('focus', function () {
            if ($(this).val().trim() === '') {
                $(this).val(th.slugGenerator($('input[name="title"]').val()));
            }
        });

        // image selector from file-manager
        $('.image-selector').on('click', '.image-selector-button', function (e) {
            e.preventDefault();

            th.imageSelectorId = $(this).offsetParent().attr('id');

            const left = (screen.width - 800) / 2;
            const top = (screen.height - 600) / 3;

            window.open(
                '/file-manager/fm-button',
                'fm',
                'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,' +
                'width=800,height=600,top=' + top + ',left=' + left
            );
        });
    }

    _initDestroyModal() {
        const modalDestroy = $('#modal-destroy');

        $('body').on('click', '[data-action="destroy"]', function (e) {
            e.preventDefault();
            const url = $(this).data('destroy-url');

            modalDestroy.modal('show');

            modalDestroy.off('click', '[data-action="confirm-destroy"]');
            modalDestroy.on('click', '[data-action="confirm-destroy"]', function (e) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        _method: 'DELETE',
                        _token: $('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function (response) {
                        if (response.redirect) {
                            window.location.href = response.redirect;
                        } else if (response.error) {
                            alert(response.error);
                        } else {
                            window.location.reload();
                        }
                    },
                    beforeSend: function () {
                        modalDestroy.find('.modal-content .block').addClass('block-mode-loading');
                    },
                    complete: function () {
                        modalDestroy.find('.modal-content .block').removeClass('block-mode-loading');
                    },
                    error: function (error) {
                        console.error(error);
                        alert(error);
                    }
                });
            });
        });
    }

    _initCkeditor() {
        if ($('#ckeditor-simple').length || $('#ckeditor-full').length) {
            CKEDITOR.on('dialogDefinition', function (ev) {
                // Take the dialog name and its definition from the event data.
                const dialogName = ev.data.name;
                const dialogDefinition = ev.data.definition;
                // Check if the definition is from the dialog we're
                // interested in (the "Table" dialog).
                if (dialogName === 'table') {
                    const infoTab = dialogDefinition.getContents('info');
                    let width = infoTab.get('txtWidth');
                    width['default'] = '100%';

                    // Remove unnecessary widgets from the 'Table Info' tab.
                    infoTab.remove('txtSummary');
                    infoTab.remove('txtCaption');
                    infoTab.remove('selHeaders');
                    // infoTab.remove('txtWidth');
                    // infoTab.remove('txtHeight');
                    // infoTab.remove('cmbAlign');
                    // infoTab.remove('txtBorder');
                    // infoTab.remove('txtCellSpace');
                    // infoTab.remove('txtCellPad');
                }
            });
        }

        if ($('#ckeditor-simple').length) {
            CKEDITOR.replace('ckeditor-simple', {
                toolbar: [
                    {
                        name: 'basic',
                        items: ['Bold', 'Italic', 'Underline', 'RemoveFormat']
                    },
                    {
                        name: 'align',
                        items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                    },
                    {
                        name: 'links',
                        items: ['Link', 'Unlink']
                    },
                    {
                        name: 'paragraph',
                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent']
                    },
                    {
                        name: 'tools',
                        items: ['Source']
                    },
                ],
                removeButtons: 'Subscript,Superscript',
                extraPlugins: 'justify',
            });
        }

        if ($('#ckeditor-full').length) {
            CKEDITOR.replace('ckeditor-full', {
                height: '350px',
                toolbar: [
                    {
                        name: 'basic',
                        items: ['Format', 'Bold', 'Italic', 'Underline', 'RemoveFormat']
                    },
                    {
                        name: 'align',
                        items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                    },
                    {
                        name: 'colors',
                        items: ['TextColor', 'BGColor']
                    },
                    {
                        name: 'links',
                        items: ['Link', 'Unlink']
                    },
                    {
                        name: 'paragraph',
                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
                    },
                    {
                        name: 'insert',
                        items: ['Image', 'Table']
                    },
                    {
                        name: 'tools',
                        items: ['Maximize', 'Source']
                    },
                ],
                removeButtons: 'Subscript,Superscript',
                format_tags: 'p;h2;h3;h4;h5',
                extraPlugins: 'colorbutton,colordialog,tableresize,tableselection,justify,image2',
                removeDialogTabs: 'image:advanced;link:advanced',
                colorButton_enableMore: true,
                // filebrowserUploadUrl: '/inside/ckeditor/upload',
                // filebrowserUploadMethod: 'form',
                filebrowserImageBrowseUrl: '/file-manager/ckeditor',
            });
        }
    }

    slugGenerator(s) {
        s = String(s);

        const options = {
            'delimiter': '-',
            'limit': undefined,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof (XRegExp) === 'undefined') ? true : false
        };

        const charMap = {
            // Kazash
            'Ә': 'A', 'Ғ': 'G', 'Қ': 'Q', 'Ө': 'O', 'Ұ': 'U', 'Ү': 'u', 'һ': '', 'І': 'I', 'Ң': 'Ng',
            'ә': 'a', 'ғ': 'g', 'қ': 'q', 'ө': 'o', 'ұ': 'u', 'ү': 'u', 'Һ': '', 'і': 'i', 'ң': 'ng',
            // Russian
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya',
        };

        // Make custom replacements
        for (var k in options.replacements) {
            s = s.replace(RegExp(k, 'g'), options.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (options.transliterate) {
            for (var k in charMap) {
                s = s.replace(RegExp(k, 'g'), charMap[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof (XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, options.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + options.delimiter + ']{2,}', 'g'), options.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, options.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + options.delimiter + '|' + options.delimiter + '$)', 'g'), '');

        return options.lowercase ? s.toLowerCase() : s;
    }
}

// Once everything is loaded
jQuery(() => {
    // Create a new instance of App
    window.One = new App();

    // Create function for set file-url from FileManager
    window.fmSetLink = function ($url) {
        const imageSelector = $('#' + One.imageSelectorId);
        imageSelector.find('.image-selector-input').attr('value', $url);
        imageSelector.parent().find('.image-selector-preview').attr('src', $url);
        imageSelector.parent().find('.image-preview').show();
    };
});
