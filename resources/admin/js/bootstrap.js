// Import all vital core JS files..
import jQuery from 'jquery';
import Cookies from 'js-cookie';
import SimpleBar from 'simplebar';
import 'bootstrap';
import 'jquery.appear';
import 'jquery-scroll-lock';

// ..and assign to window the ones that need it
window.$ = window.jQuery = jQuery;
window.SimpleBar = SimpleBar;
window.Cookies = Cookies;
