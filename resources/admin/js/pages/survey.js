$(() => {
    const questionFactorSelect = $('select#question_factor_id');
    const questionIndicatorSelect = $('select#question_indicator_id');
    const questionSubIndicatorSelect = $('select#question_sub_indicator_id');
    const questionTypeSelect = $('select#question_type');
    const addAnswerButton = $('#add-answer');
    const answersTable = $('#answers-table');
    const answersTableWrapper = $('#answers-table-wrapper');
    const answerFormWrapper = $('#answer-form-wrapper');
    const answerRowTemplate = $('#answer-row-template');

    const dropSelect = (element) => {
        element.html('<option selected disabled>Select...</option>');
        element.prop('disabled', true);
    };

    const updateSelect = (element, data) => {
        let listItems = '';

        data.forEach(function (item) {
            listItems += '<option value="' + item.id + '">' + item.name + '</option>';
        });

        element.append(listItems);
        element.prop('disabled', false);
    };

    questionFactorSelect.on('change', function (e) {
        e.preventDefault();

        dropSelect(questionIndicatorSelect);
        dropSelect(questionSubIndicatorSelect);

        $.get(
            '/inside/survey/indicators/load',
            {
                factor_id: $(this).val()
            },
            function (data) {
                updateSelect(questionIndicatorSelect, data);
            }
        )
        .fail(function (data) {
            alert('Error occurred while getting data!');
            console.error(data);
        });
    });

    questionIndicatorSelect.on('change', function (e) {
        e.preventDefault();

        dropSelect(questionSubIndicatorSelect);

        $.get(
            '/inside/survey/sub-indicators/load',
            {
                indicator_id: $(this).val()
            },
            function (data) {
                updateSelect(questionSubIndicatorSelect, data);
            }
        )
        .fail(function (data) {
            alert('Error occurred while getting data!');
            console.error(data);
        });
    });

    questionTypeSelect.on('change', function (e) {
        e.preventDefault();

        if ($(this).val() === 'multi_select' || $(this).val() === 'single_select') {
            answersTableWrapper.show();
            answerFormWrapper.hide();
        } else if ($(this).val() === 'input') {
            answerFormWrapper.show();
            answersTableWrapper.hide();
        }
    })

    answersTable.on('click', '.answer-row-remove', function () {
        $(this).closest('tr.answer-row').remove();
    });

    let answerId = parseInt($('.answer-row:last .answer-id').val());

    addAnswerButton.on('click', function (e) {
        e.preventDefault();

        answerId++;

        const answerRow = answerRowTemplate.clone();

        answerRow.removeClass('hidden');
        answerRow.attr('id', 'answer-row-' + answerId);
        answerRow.find('th:first').html(answerId);

        answerRow.find('input').each(function () {
            const name = $(this).data('name');
            $(this).attr('name', `answers[${answerId}][${name}]`);

            if ($(this).hasClass('answer-id')) {
                $(this).val(answerId);
            }
        });

        answersTable.find('tbody').append(answerRow);
    })
});
