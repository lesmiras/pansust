window.$ = window.jQuery = require('jquery');
window.Vue = require('vue').default;
window.JSShare = require('./vendor/jsshare');
require('./vendor/bootstrap-util');
require('./vendor/bootstrap-modal');

import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

Vue.component('survey', require('./components/Survey.vue').default);
Vue.component('comment-form', require('./components/CommentForm.vue').default);

const app = new Vue({
    el: '#app',
});

$(function () {
    $('.share-button').on('click', function (e) {
        e.preventDefault();

        JSShare.go(this);
    });

    $('body').on('click', '.wiki-list__item-title', function () {
        const wrapper = $(this).closest('.wiki-list__item');

        console.log($(this));

        if (wrapper.hasClass('wiki-list__item--open')) {
            wrapper.removeClass('wiki-list__item--open');
            wrapper.find('.wiki-list__item-body').slideUp();
        } else {
            wrapper.addClass('wiki-list__item--open');
            wrapper.find('.wiki-list__item-body').slideDown();
        }
    })

    $('button.mobile-nav-toggle').on('click', function () {
        const mobileBox = $('.header-mobile-box');

        if ($(this).hasClass('is-active')) {
            $(this).removeClass('is-active')
            mobileBox.slideUp();
        } else {
            $(this).addClass('is-active')
            mobileBox.slideDown();
        }
    })

    $(window).on('resize', function () {
        if ($(window).width() > 720) {
            $('.header-mobile-box').hide();
            $('button.mobile-nav-toggle').removeClass('is-active');
        }
    });

    // button load-more articles
    $('[data-load-articles]').on('click', function (e) {
        e.preventDefault();

        const button = $(this);
        const wrapper = $('.article-blocks');
        const type = $(this).data('load-articles');
        const order = $(this).data('order');
        const page = $(this).data('page') + 1;

        $.ajax({
            url: `/${type}`,
            data: {
                page: page,
                order: order
            },
            method: 'GET',
            beforeSend: function () {
                button.addClass('is-loading');
            },
            complete: function () {
                button.removeClass('is-loading');
            },
            success: function (response) {
                if (response.html) {
                    wrapper.append(response.html);
                }

                if (response.currentPage >= response.lastPage) {
                    button.remove();
                } else {
                    button.data('page', page);
                }
            },
            error: function (error) {
                console.error(error);
                alert('Something went wrong!');
            }
        });
    });

    // button load-more comments
    $('[data-load-comments]').on('click', function (e) {
        e.preventDefault();

        const button = $(this);
        const wrapper = $('.comment-list');
        const type = $(this).data('load-comments');
        const id = $(this).data('id');
        const page = $(this).data('page') + 1;

        $.ajax({
            url: '/comments/load',
            data: {
                type: type,
                id: id,
                page: page,
            },
            method: 'GET',
            beforeSend: function () {
                button.addClass('is-loading');
            },
            complete: function () {
                button.removeClass('is-loading');
            },
            success: function (response) {
                if (response.html) {
                    wrapper.append(response.html);
                }

                if (response.currentPage >= response.lastPage) {
                    button.remove();
                } else {
                    button.data('page', page);
                }
            },
            error: function (error) {
                console.error(error);
                alert('Something went wrong!');
            }
        });
    });

    // button load-more comments
    $('[data-load-search]').on('click', function (e) {
        e.preventDefault();

        const button = $(this);
        const type = $(this).data('load-search');
        const wrapper = $(`#tab-${type} .search-result-items`);
        const query = $(this).data('query');
        const page = $(this).data('page') + 1;

        $.ajax({
            url: '/search/load',
            data: {
                type: type,
                query: query,
                page: page,
            },
            method: 'GET',
            beforeSend: function () {
                button.addClass('is-loading');
            },
            complete: function () {
                button.removeClass('is-loading');
            },
            success: function (response) {
                if (response.html) {
                    wrapper.append(response.html);
                }

                if (response.currentPage >= response.lastPage) {
                    button.remove();
                } else {
                    button.data('page', page);
                }
            },
            error: function (error) {
                console.error(error);
                alert('Something went wrong!');
            }
        });
    });

    // ajax-form submit
    $('body').on('submit', 'form.ajax-form', function (e) {
        e.preventDefault();

        const form = $(this);
        const formData = new FormData(this);
        const modal = form.closest('.modal');

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                if (response.success) {
                    if (modal.hasClass('modal-subscribe')) {
                        const successTemplate = modal.find('.success-template').clone().html();
                        modal.find('.modal-message').html(successTemplate);
                    } else {
                        $('#modal-success').modal('show');
                    }
                }

                if (response.redirect) {
                    window.location = response.redirect;
                }

                form.trigger('reset');
            },
            error: function (response) {
                if (response.status === 422 || response.status === 423) {
                    for (let inputName in response.responseJSON.errors) {
                        if (inputName === 'recaptcha') {
                            form.find('#recaptcha-error').html(response.responseJSON.errors[inputName][0])
                        } else {
                            const inputBlock = form.find(`[name=${inputName}]`).parent('.input-block');
                            inputBlock.addClass('has-error');
                            inputBlock.find('.input-error').html(response.responseJSON.errors[inputName][0]);
                        }
                    }
                } else {
                    $('#modal-fail').modal('show');
                    form.trigger('reset');
                }
            },
            beforeSend: function () {
                form.addClass('is-loading');
                form.find('.input-block').removeClass('has-error');
                form.find('.input-error').html('');
            },
            complete: function () {
                form.removeClass('is-loading');
            }
        })
    });

    // tab nav init
    $('[data-tab-nav]').each(function() {
        let tabContent = $(this).data('tab-nav');
        let tabContentElem = $('[data-tab-content="' + tabContent + '"]');

        $(this).find('[data-tab-item]:first-child').addClass('is-active');
        tabContentElem.children('[data-tab-panel]:first-child').show().addClass('is-active');

        $(this).on('click', 'a', function (e) {
            e.preventDefault();
            const hash = $(this).attr('href');

            if (hash === '#main') {
                tabContentElem.children('[data-tab-panel]').fadeIn().addClass('is-active');
            } else if ($(hash).length !== 0) {
                tabContentElem.children('[data-tab-panel]').hide();
                tabContentElem.children('[data-tab-panel]').removeClass('is-active');
                tabContentElem.find(hash).fadeIn().addClass('is-active');
            }

            $(this).parent().siblings().removeClass('is-active');
            $(this).parent().addClass('is-active');
            $(this).addClass('is-active');
        });
    });


    const satGraph = document.getElementById('sat-graph');

    if (satGraph) {
        const ctx = satGraph.getContext('2d');
        const totalENV = satGraph.getAttribute('data-total-env');
        const totalECO = satGraph.getAttribute('data-total-eco');
        const totalSF = satGraph.getAttribute('data-total-sf');

        const chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['ENV', 'ECO', 'S&F'],
                datasets: [
                    {
                        backgroundColor: ['rgb(128, 197, 218)', 'rgb(197, 249, 188)', 'rgb(196, 196, 196)'],
                        borderColor: ['rgb(128, 197, 218)', 'rgb(197, 249, 188)', 'rgb(196, 196, 196)'],
                        data: [totalENV, totalECO, totalSF],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: false,
                    }
                }
            }
        });
    }
});