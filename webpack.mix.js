const mix = require('laravel-mix');

mix
    /* ADMIN: CSS */
    .sass('resources/admin/sass/main.scss', 'public/admin/css/oneui.css')
    /* ADMIN: JS */
    .js('resources/admin/js/app.js', 'public/admin/js/app.js')

    /* SITE: CSS */
    .sass('resources/assets/sass/app.scss', 'public/assets/css')
    /* SITE: JS */
    .js('resources/assets/js/app.js', 'public/assets/js')

    .sourceMaps(false, 'source-map')
    .vue()

    /* Options */
    .options({
        processCssUrls: false
    })
;