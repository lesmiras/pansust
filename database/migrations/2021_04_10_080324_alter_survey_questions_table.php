<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSurveyQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_questions', function (Blueprint $table) {
            $table->foreignId('factor_id')
                ->after('id')
                ->references('id')
                ->on('survey_factors');
            $table->foreignId('indicator_id')
                ->after('factor_id')
                ->references('id')
                ->on('survey_indicators');
            $table->foreignId('sub_indicator_id')
                ->after('indicator_id')
                ->references('id')
                ->on('survey_sub_indicators');
            $table->tinyInteger('is_published')->default(0)->after('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_questions', function (Blueprint $table) {
            $table->dropForeign('survey_questions_factor_id_foreign');
            $table->dropForeign('survey_questions_indicator_id_foreign');
            $table->dropForeign('survey_questions_sub_indicator_id_foreign');
            $table->dropColumn('factor_id');
            $table->dropColumn('indicator_id');
            $table->dropColumn('sub_indicator_id');
            $table->dropColumn('is_published');
        });
    }
}
