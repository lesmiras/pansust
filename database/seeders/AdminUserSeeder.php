<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = User::create([
            'name' => 'Super Administrator',
            'email' => 'admin@pansust.org',
            'password' => '$2y$10$xvYq07Sb/cr6wQWtBIuba.q33Muhy.sthNObGDXdUz9fwdJv8zxUC',
        ]);

        $administrator->assignRole('administrator');

        $moderator = User::create([
            'name' => 'Super Moderator',
            'email' => 'moderator@pansust.org',
            'password' => '$2y$10$YjnGAXQor8zl5XKR96X42eEYKOkeJjs6JtVj/Jzl982o4PeYWdQlK',
        ]);

        $moderator->assignRole('moderator');
    }
}
