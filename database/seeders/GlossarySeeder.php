<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\Glossary;
use Illuminate\Database\Seeder;

class GlossarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Glossary::factory()->count(50)->create();
    }
}
