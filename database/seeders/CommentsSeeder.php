<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Event;
use App\Models\Forum;
use App\Models\News;
use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news = News::all();

        foreach ($news as $newsItem) {
            Comment::factory()->create([
                'commentable_id' => $newsItem->id,
                'commentable_type' => News::class,
            ]);
        }

        $events = Event::all();

        foreach ($events as $event) {
            Comment::factory()->create([
                'commentable_id' => $event->id,
                'commentable_type' => Event::class,
            ]);
        }

        $forums = Forum::all();

        foreach ($forums as $forum) {
            Comment::factory()->create([
                'commentable_id' => $forum->id,
                'commentable_type' => Forum::class,
            ]);
        }
    }
}
