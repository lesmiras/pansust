<?php

namespace Database\Seeders;

use App\Models\SurveyFactor;
use App\Models\SurveyIndicator;
use App\Models\SurveySubIndicator;
use Illuminate\Database\Seeder;

class SurveyFactorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factors = [
            [
                'name'   => 'ENV',
                'weight' => 4.57,
            ],
            [
                'name'   => 'ECO',
                'weight' => 4.41,
            ],
            [
                'name'   => 'S&F',
                'weight' => 4.51,
            ],
        ];

        foreach ($factors as $factor) {
            SurveyFactor::create([
                'name'   => $factor['name'],
                'weight' => $factor['weight'],
            ]);
        }

        $factors = SurveyFactor::all();

        $indicators = [
            [
                'factor_id' => $factors->where('name', 'ENV')->first()->id,
                'name'      => 'ENV1',
                'weight'    => 4.65,
            ],
            [
                'factor_id' => $factors->where('name', 'ENV')->first()->id,
                'name'      => 'ENV2',
                'weight'    => 3.99,
            ],
            [
                'factor_id' => $factors->where('name', 'ENV')->first()->id,
                'name'      => 'ENV3',
                'weight'    => 4.15,
            ],
            [
                'factor_id' => $factors->where('name', 'ENV')->first()->id,
                'name'      => 'ENV4',
                'weight'    => 4.58,
            ],
            [
                'factor_id' => $factors->where('name', 'ECO')->first()->id,
                'name'      => 'ECO1',
                'weight'    => 4.47,
            ],
            [
                'factor_id' => $factors->where('name', 'ECO')->first()->id,
                'name'      => 'ECO2',
                'weight'    => 4.13,
            ],
            [
                'factor_id' => $factors->where('name', 'S&F')->first()->id,
                'name'      => 'S&F1',
                'weight'    => 4.53,
            ],
            [
                'factor_id' => $factors->where('name', 'S&F')->first()->id,
                'name'      => 'S&F2',
                'weight'    => 3.86,
            ],
            [
                'factor_id' => $factors->where('name', 'S&F')->first()->id,
                'name'      => 'S&F3',
                'weight'    => 3.86,
            ],
            [
                'factor_id' => $factors->where('name', 'S&F')->first()->id,
                'name'      => 'S&F4',
                'weight'    => 3.88,
            ],
            [
                'factor_id' => $factors->where('name', 'S&F')->first()->id,
                'name'      => 'S&F5',
                'weight'    => 4.09,
            ],
        ];

        foreach ($indicators as $indicator) {
            SurveyIndicator::create([
                'factor_id' => $indicator['factor_id'],
                'name'      => $indicator['name'],
                'weight'    => $indicator['weight'],
            ]);
        }

        $indicators = SurveyIndicator::all();

        $subIndicators = [
            [
                'indicator_id' => $indicators->where('name', 'ENV1')->first()->id,
                'name'         => 'ENV1.1',
                'weight'       => 4.47,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV1')->first()->id,
                'name'         => 'ENV1.2',
                'weight'       => 4.56,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV1')->first()->id,
                'name'         => 'ENV1.3',
                'weight'       => 4.08,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV2')->first()->id,
                'name'         => 'ENV2.1',
                'weight'       => 4.03,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV2')->first()->id,
                'name'         => 'ENV2.2',
                'weight'       => 4.05,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV2')->first()->id,
                'name'         => 'ENV2.3',
                'weight'       => 3.51,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV2')->first()->id,
                'name'         => 'ENV2.4',
                'weight'       => 4.2,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV2')->first()->id,
                'name'         => 'ENV2.5',
                'weight'       => 3.95,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV2')->first()->id,
                'name'         => 'ENV2.6',
                'weight'       => 3.71,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV3')->first()->id,
                'name'         => 'ENV3.1',
                'weight'       => 4.07,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV3')->first()->id,
                'name'         => 'ENV3.2',
                'weight'       => 4.31,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV4')->first()->id,
                'name'         => 'ENV4.1',
                'weight'       => 4.53,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV4')->first()->id,
                'name'         => 'ENV4.2',
                'weight'       => 3.87,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV4')->first()->id,
                'name'         => 'ENV4.3',
                'weight'       => 3.53,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ENV4')->first()->id,
                'name'         => 'ENV4.4',
                'weight'       => 3.79,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ECO1')->first()->id,
                'name'         => 'ECO1.1',
                'weight'       => 4.33,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ECO1')->first()->id,
                'name'         => 'ECO1.2',
                'weight'       => 4.63,
            ],
            [
                'indicator_id' => $indicators->where('name', 'ECO2')->first()->id,
                'name'         => 'ECO2.1',
                'weight'       => 4.13,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.1',
                'weight'       => 4.61,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.2',
                'weight'       => 4.61,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.3',
                'weight'       => 4.61,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.4',
                'weight'       => 4.31,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.5',
                'weight'       => 4.53,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.6',
                'weight'       => 4.69,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F1')->first()->id,
                'name'         => 'S&F1.7',
                'weight'       => 4.43,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F2')->first()->id,
                'name'         => 'S&F2.1',
                'weight'       => 3.92,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F2')->first()->id,
                'name'         => 'S&F2.2',
                'weight'       => 3.73,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F3')->first()->id,
                'name'         => 'S&F3.1',
                'weight'       => 4.64,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F3')->first()->id,
                'name'         => 'S&F3.2',
                'weight'       => 4.28,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F4')->first()->id,
                'name'         => 'S&F4.1',
                'weight'       => 4.16,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F4')->first()->id,
                'name'         => 'S&F4.2',
                'weight'       => 4.09,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F5')->first()->id,
                'name'         => 'S&F5.1',
                'weight'       => 4.23,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F5')->first()->id,
                'name'         => 'S&F5.2',
                'weight'       => 3.93,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F5')->first()->id,
                'name'         => 'S&F5.3',
                'weight'       => 3.71,
            ],
            [
                'indicator_id' => $indicators->where('name', 'S&F5')->first()->id,
                'name'         => 'S&F5.4',
                'weight'       => 4.12,
            ],
        ];

        foreach ($subIndicators as $subIndicator) {
            SurveySubIndicator::create([
                'indicator_id' => $subIndicator['indicator_id'],
                'name'         => $subIndicator['name'],
                'weight'       => $subIndicator['weight'],
            ]);
        }
    }
}
