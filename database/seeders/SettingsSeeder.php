<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'key'   => 'homeTitle',
                'value' => '“To make buildings sustainable during pandemic, PANSUST suggests developing cultural and urban-specific building characteristics and sustainability functions for times when millions of people are isolated and have to work from home as in the current COVID-19 pandemic.”',
            ],
            [
                'key'   => 'social_link_instagram',
                'value' => 'https://instagram.com/',
            ],
            [
                'key'   => 'social_link_facebook',
                'value' => 'http://facebook.com/',
            ],
            [
                'key'   => 'social_link_linkedin',
                'value' => 'https://www.linkedin.com/',
            ],
            [
                'key'   => 'contacts_primary_email',
                'value' => 'contact.pansust@nu.edu.kz',
            ],
            [
                'key'   => 'contacts_pi_emails',
                'value' => 'ferhat.karaca@nu.edu.kz,mert.guney@nu.edu.kz,ali.turkyilmaz@nu.edu.kz',
            ],
        ];

        foreach ($items as $item) {
            Setting::create($item);
        }
    }
}
