<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administratorRole = Role::create([
            'name' => 'administrator',
        ]);

        $administratorRole->syncPermissions(Permission::pluck('id', 'id')->all());

        $moderatorRole = Role::create([
            'name' => 'moderator',
        ]);

        $moderatorRole->givePermissionTo('admin-panel-access');
    }
}
