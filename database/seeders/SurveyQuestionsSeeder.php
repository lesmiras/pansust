<?php

namespace Database\Seeders;

use App\Models\SurveyFactor;
use App\Models\SurveyIndicator;
use App\Models\SurveyQuestion;
use App\Models\SurveySubIndicator;
use Illuminate\Database\Seeder;

class SurveyQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factors       = SurveyFactor::all();
        $indicators    = SurveyIndicator::all();
        $subIndicators = SurveySubIndicator::all();

        $questions = [
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV1.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is your level of satisfaction with the building’s energy consumption?',
                'answers'          => [
                    [
                        'title'  => 'Not happy',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Marginally happy',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Happy',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'Very Happy',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Extremely Happy',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV1.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Please evaluate the current performance of your building insulation by comparing to an “ideal” case for insulation only:',
                'answers'          => [
                    [
                        'title'  => 'Extremely poor',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Poor',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'Optimal',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Better than optimal',
                        'weight' => 7,
                    ],
                    [
                        'title'  => 'Excellent ',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV1.3')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What percentage of your building’s energy is covered by renewable energy technology annually?',
                'answers'          => [
                    [
                        'title'  => 0,
                        'weight' => 0,
                    ],
                    [
                        'title'  => '10%',
                        'weight' => 2,
                    ],
                    [
                        'title'  => '30%',
                        'weight' => 4,
                    ],
                    [
                        'title'  => '50%',
                        'weight' => 6,
                    ],
                    [
                        'title'  => '70%',
                        'weight' => 8,
                    ],
                    [
                        'title'  => '90% or more',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Does your building retrofit existing buildings?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Does your building minimize the creation of new impervious surfaces (parking, building layout, travel lanes, etc.)?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Does your building consider efficient watershed plans?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Did your building consider energy implications and carbon emissions in site selection?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Did your building apply best practices for erosion control?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Does your building use native plants and remove any invasive species?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Does your building minimize habitat disturbance?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Does your building reduce, control, or treat surface runoff through effective stormwater practices?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1.25,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Securing of flora and fauna habitat',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Control of surface water runoff',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Climate adaptation improvement (e.g. decrease of heat emission, carbon binding)',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Improvement of private recreational areas (gardens, courtyards)',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Improving public green areas (e.g. parks)',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Improving recreational areas (e.g., forests, landscape parks)',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Protection of agricultural areas',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Evaluate soil sealing (e.g. asphalt, concrete) consider:<br>Protection of ecologically valuable fertile soils',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.3')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is the percentage reuse of previously built or contaminated areas? (%)',
                'answers'          => [
                    'weight'    => 0.1,
                    'type'      => 'integer',
                    'min_value' => 0,
                    'max_value' => 100,
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Ecological Protection of the site:<br>Does the construciton site satisfy the requirements of an ecological survey?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Ecological Protection of the site:<br>Presence of habitats within 100 m (e.g. woodlands, water sources, wetlands, flower-rich lands)',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Ecological Protection of the site:<br>Destruction of mature hedgerow or trees older than 10 years',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Ecological Protection of the site:<br>Presence of one of the Nature Reserves of the region within 2 km',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.5')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is the proportion of rehabilitated land to effective land?',
                'answers'          => [
                    'weight'    => 10,
                    'type'      => 'float',
                    'min_value' => 0,
                    'max_value' => 1,
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV2.6')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is the ratio of surface area covered by native plants to the total vegetative area?',
                'answers'          => [
                    'weight'    => 10,
                    'type'      => 'float',
                    'min_value' => 0,
                    'max_value' => 1,
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV3')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV3.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is the percentage of reused and recycled materials in the total amount of material used? (X)',
                'answers'          => [
                    [
                        'title'  => 'X ≤3%',
                        'weight' => 0,
                    ],
                    [
                        'title'  => '3%< X ≤8%',
                        'weight' => 2,
                    ],
                    [
                        'title'  => '8%< X ≤12%',
                        'weight' => 6,
                    ],
                    [
                        'title'  => '12%< X ≤15%',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'X > 15%',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV3')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV3.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Do you have a space in the building or adjacent to the building suitable for storage of wastes/recyclables?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV3')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV3.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Do you have separate containers for different categories of household waste?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV3')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV3.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is the condition of the containers?',
                'answers'          => [
                    [
                        'title'  => 'Excellent',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Good',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Moderate',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Poor',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Does not exist',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV4')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV4.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Please compare the average water consumption per person in the building (B) with the average water consumption in the region (R).',
                'answers'          => [
                    [
                        'title'  => 'B is more or equal to R',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'B is 1 m<sup>3</sup> less than R',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'B is 2 m<sup>3</sup> less than R',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'B is 3 m<sup>3</sup> less than R',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'B is 4 m<sup>3</sup> less than R',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'B is 5 m<sup>3</sup> (or more) less than R',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV4')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV4.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is the percentage of reused or recycled water in the building?',
                'answers'          => [
                    'weight'    => 0.1,
                    'type'      => 'integer',
                    'min_value' => 0,
                    'max_value' => 100,
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV4')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV4.3')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What percentage of rain and stormwater is consumed?',
                'answers'          => [
                    [
                        'title'  => '0%',
                        'weight' => 0,
                    ],
                    [
                        'title'  => '1-25%',
                        'weight' => 3,
                    ],
                    [
                        'title'  => '25-50%',
                        'weight' => 5,
                    ],
                    [
                        'title'  => '>50%',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ENV')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ENV4')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ENV4.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Is there any type of separation of black water (toilet water) from grey water?',
                'answers'          => [
                    [
                        'title'  => 'Yes',
                        'weight' => 10,
                    ],
                    [
                        'title'  => 'No',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ECO')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ECO1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ECO1.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is a ratio between the purchase cost of 1 m<sup>2</sup> of the building and an average market cost of 1 m<sup>2</sup> of residential buildings in the area?',
                'answers'          => [
                    [
                        'title'  => '>125%',
                        'weight' => 0,
                    ],
                    [
                        'title'  => '125%-115%',
                        'weight' => 2,
                    ],
                    [
                        'title'  => '115%-105%',
                        'weight' => 3,
                    ],
                    [
                        'title'  => '105%-95%',
                        'weight' => 4,
                    ],
                    [
                        'title'  => '95%-85%',
                        'weight' => 5,
                    ],
                    [
                        'title'  => '85%-80%',
                        'weight' => 6,
                    ],
                    [
                        'title'  => '80%-75%',
                        'weight' => 7,
                    ],
                    [
                        'title'  => '75%-70%',
                        'weight' => 8,
                    ],
                    [
                        'title'  => '70%-65%',
                        'weight' => 9,
                    ],
                    [
                        'title'  => '<65%',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ECO')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ECO1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ECO1.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Please, compare average operational cost per person of the region with the operational cost per person of the building, and rate your satisfaction level.',
                'answers'          => [
                    [
                        'title'  => 'Not happy',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Marginally happy',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Happy',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'Very Happy',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Extremely happy',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ECO')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ECO2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ECO2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is the ratio of local goods to total goods used during operational stage of the building?',
                'answers'          => [
                    'weight'    => 5,
                    'type'      => 'float',
                    'min_value' => '0',
                    'max_value' => '1',
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'ECO')->first()->id,
                'indicator_id'     => $indicators->where('name', 'ECO2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'ECO2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is the ratio of local services to total services used during operational stage of the building?',
                'answers'          => [
                    'weight'    => 5,
                    'type'      => 'float',
                    'min_value' => 0,
                    'max_value' => 1,
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'How many days building is working with mechanical ventilation?',
                'answers'          => [
                    [
                        'title'  => '311-365 days',
                        'weight' => 0,
                    ],
                    [
                        'title'  => '275-310 days',
                        'weight' => 2,
                    ],
                    [
                        'title'  => '238-274 days',
                        'weight' => 3,
                    ],
                    [
                        'title'  => '202-237 days',
                        'weight' => 4,
                    ],
                    [
                        'title'  => '165-201 days',
                        'weight' => 5,
                    ],
                    [
                        'title'  => '129-164 days',
                        'weight' => 6,
                    ],
                    [
                        'title'  => '92-128 days',
                        'weight' => 7,
                    ],
                    [
                        'title'  => '56-91 days',
                        'weight' => 8,
                    ],
                    [
                        'title'  => '20-55 days',
                        'weight' => 9,
                    ],
                    [
                        'title'  => '0-19 days',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is the level of materials certification?',
                'answers'          => [
                    [
                        'title'  => 'No certified materials were used',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Standard certifications are not provided',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'All materials satisfy the standards',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Some form of green certification was acquired',
                        'weight' => 7,
                    ],
                    [
                        'title'  => 'All necessary green certification was acquired',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.3')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'How do you find building’s temperature?',
                'answers'          => [
                    [
                        'title'  => 'Very Cold',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Cold',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Cool',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'Slightly cool',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Neutral',
                        'weight' => 10,
                    ],
                    [
                        'title'  => 'Slightly warm',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Warm',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'Hot',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Very hot',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Is it visually comfortable at the building?',
                'answers'          => [
                    [
                        'title'  => 'Not comfortable in any place in the building',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Several complaints about private and common places in the building',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'Comfortable in flats but some complaints on common areas',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Good level of illumination in all the parts of the building',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Very good level of illumination in all the parts of the building',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.5')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Is it acoustically comfortable at the building in terms of soundproofing with outdoors?',
                'answers'          => [
                    [
                        'title'  => 'Very good',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Satisfactory',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'Not satisfactory',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Extremely not satisfactory',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.5')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Soundproofing with indoors with other flats',
                'answers'          => [
                    [
                        'title'  => 'Very good',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Satisfactory',
                        'weight' => 1.5,
                    ],
                    [
                        'title'  => 'Not satisfactory',
                        'weight' => 0.5,
                    ],
                    [
                        'title'  => 'Extremely not satisfactory',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.5')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Soundproofing with indoors within the flat (other rooms)',
                'answers'          => [
                    [
                        'title'  => 'Very good',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'Satisfactory',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Not satisfactory',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.5')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Number of appliances  (washing machine, etc.) per room',
                'answers'          => [
                    [
                        'title'  => 'No appliance',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'One appliance',
                        'weight' => 0.5,
                    ],
                    [
                        'title'  => 'More than two appliances',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.6')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Do you have any complaints on your health conditions?',
                'answers'          => [
                    [
                        'title'  => 'No',
                        'weight' => 2.5,
                    ],
                    [
                        'title'  => 'Yes, one',
                        'weight' => 1.5,
                    ],
                    [
                        'title'  => 'Yes, several',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.6')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'How many people are per m<sup>2</sup>?',
                'answers'          => [
                    [
                        'title'  => 'Less than 4',
                        'weight' => 2.5,
                    ],
                    [
                        'title'  => 'Between 4 and 6',
                        'weight' => 1.5,
                    ],
                    [
                        'title'  => 'More than 6',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.6')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'How do you find humidity in the building?',
                'answers'          => [
                    [
                        'title'  => 'Comfortable all the time',
                        'weight' => 2.5,
                    ],
                    [
                        'title'  => 'Comfortable most of the time',
                        'weight' => 1.5,
                    ],
                    [
                        'title'  => 'Sometimes uncomfortable',
                        'weight' => 0.5,
                    ],
                    [
                        'title'  => 'Always uncomfortable',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.6')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'How do you find facility ventilation system?',
                'answers'          => [
                    [
                        'title'  => 'Excellent air mixing',
                        'weight' => 2.5,
                    ],
                    [
                        'title'  => 'Acceptable air mixing',
                        'weight' => 1.5,
                    ],
                    [
                        'title'  => 'Poor air mixing – the presence of the dead spots with poor air movement',
                        'weight' => 0.5,
                    ],
                    [
                        'title'  => 'Very poor',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F1')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F1.7')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is window-to-floor area ratio?',
                'answers'          => [
                    [
                        'title'  => 'No availability of natural light',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Less than 4%',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Between 4 and 5%',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Between 5 and 10%',
                        'weight' => 7,
                    ],
                    [
                        'title'  => 'Between 10 and 15%',
                        'weight' => 9,
                    ],
                    [
                        'title'  => 'More than 15%',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F2.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is % of building orientation?',
                'answers'          => [
                    [
                        'title'  => 'Less than 40%',
                        'weight' => 10,
                    ],
                    [
                        'title'  => 'Between 40 and 60%',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Between 60 and 80%',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'Between 80 and 95%',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Between 95 and 99%',
                        'weight' => 2,
                    ],
                    [
                        'title'  => '100%',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F2')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F2.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_MULTI_SELECT,
                'question'         => 'Any passive systems in the building?',
                'answers'          => [
                    [
                        'title'  => 'Use of any means of solar energy',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Availability of a passive ventilation system (different than regular windows)',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Other passive methods of technologies (e.g. humidity control, odour control, air-tight construction, super insulation, good orientation of the building, self-shading design, etc.)',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'None',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F3')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F3.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is the level of occupant safety? (occupational illnesses and injuries, exposure to hazardous materials, accidental falls, slips, trips, and other injury possibilities, nuisance control including mental as well as physical health)',
                'answers'          => [
                    [
                        'title'  => 'Unacceptable (if more than 10 min)',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Poor (8-9 min)',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Average (5-8 min)',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Very good (2-5 min)',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Outstanding (0-2 min)',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F3')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F3.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'What is the level of general accessibility and accessibility for disabled people? (in terms of per cent difference in travel time from a starting location to a target location)',
                'answers'          => [
                    [
                        'title'  => 'Unacceptable (if more than 91%)',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Poor (81-90%)',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Average (51-80%)',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Very good (21-50%)',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Outstanding (0-20%)',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F4')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F4.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'How do you find availability and accessibility to social areas?',
                'answers'          => [
                    [
                        'title'  => 'Unacceptable',
                        'weight' => 0,
                    ],
                    [
                        'title'  => 'Poor',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Average',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Very good',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Outstanding',
                        'weight' => 10,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F4')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F4.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_SINGLE_SELECT,
                'question'         => 'Is ratio of "usable area of building" to "gross area of building"',
                'answers'          => [
                    [
                        'title'  => 'More than 80%?',
                        'weight' => 10,
                    ],
                    [
                        'title'  => 'Between 75 and 79%?',
                        'weight' => 9,
                    ],
                    [
                        'title'  => 'Between 70 and 74%?',
                        'weight' => 8,
                    ],
                    [
                        'title'  => 'Between 65 and 69%?',
                        'weight' => 7,
                    ],
                    [
                        'title'  => 'Between 60 and 64%?',
                        'weight' => 6,
                    ],
                    [
                        'title'  => 'Between 55 and 59%?',
                        'weight' => 5,
                    ],
                    [
                        'title'  => 'Between 50 and 54%?',
                        'weight' => 4,
                    ],
                    [
                        'title'  => 'Between 45 and 49%?',
                        'weight' => 3,
                    ],
                    [
                        'title'  => 'Between 40 and 44%?',
                        'weight' => 2,
                    ],
                    [
                        'title'  => 'Between 35 and 39%?',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Less than 34%?',
                        'weight' => 0,
                    ],
                ],
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is time spent to walk from the building to the bus stop? (in minutes, please do not write 0)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'integer',
                    'min_value' => 1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.1')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'What is a number of bus routes passing through the bus stop?',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'integer',
                    'min_value' => 0,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are schools? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are hospitals? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are supermarkets/stores? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are banks/ATM? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are public transport stops? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are parks? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.2')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How far are shopping malls? (in km)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'number',
                    'min_value' => 0.1,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.3')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How many times per month householders are attending local amenities by walking/cycling ? (0-31)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'integer',
                    'min_value' => 0,
                    'max_value' => 31,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.3')->first()->id,
                'type'             => SurveyQuestion::TYPE_INPUT,
                'question'         => 'How many times per month householders are attending local amenities by driving a car? (1-31, please do not put zero)',
                'answers'          => [
                    'weight'    => 1,
                    'type'      => 'integer',
                    'min_value' => 1,
                    'max_value' => 31,
                ]
            ],
            [
                'factor_id'        => $factors->where('name', 'S&F')->first()->id,
                'indicator_id'     => $indicators->where('name', 'S&F5')->first()->id,
                'sub_indicator_id' => $subIndicators->where('name', 'S&F5.4')->first()->id,
                'type'             => SurveyQuestion::TYPE_MULTI_SELECT,
                'question'         => 'Are any of the following services present? Check all that apply',
                'answers'          => [
                    [
                        'title'  => 'Fire and Security Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Intrusion Alarm Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Elevator Phone Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Building Automation Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Lighting Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'HVAC Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Critical Point Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Sanitation Monitoring',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Air Quality',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'Water Hygiene',
                        'weight' => 1,
                    ],
                    [
                        'title'  => 'None of the above',
                        'weight' => 0,
                    ],
                ]
            ],
        ];

        foreach ($questions as $question) {
            $answerId = 1;

            if (in_array($question['type'], [SurveyQuestion::TYPE_SINGLE_SELECT, SurveyQuestion::TYPE_MULTI_SELECT])) {
                foreach ($question['answers'] as &$answer) {
                    $answer = ['id' => $answerId] + $answer;
                    // $answer['id'] = $answerId;
                    $answerId++;
                }
            }

            SurveyQuestion::create([
                'factor_id'        => $question['factor_id'],
                'indicator_id'     => $question['indicator_id'],
                'sub_indicator_id' => $question['sub_indicator_id'],
                'question'         => $question['question'],
                'type'             => $question['type'],
                'answers'          => $question['answers'],
                'multiplier'       => $question['multiplier'] ?? null,
            ]);
        }
    }
}
