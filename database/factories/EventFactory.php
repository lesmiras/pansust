<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'        => $this->faker->sentence(),
            'slug'         => $this->faker->unique()->slug,
            'is_published' => random_int(0, 1),
            'published_at' => now(),
            'excerpt'      => $this->faker->text(80),
            'content'      => $this->faker->text(250),
        ];
    }
}
