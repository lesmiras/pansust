<?php

Route::group(['prefix' => 'inside', 'as' => 'admin.'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    });

    Route::group(['middleware' => ['auth', 'admin_panel_access']], function () {
        Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');

        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/file-manager', 'FilemanagerController@index')->name('fm.index');
        Route::resource('/users', 'UsersController')->except('show');
        Route::resource('/roles', 'RolesController')->except('show');
        Route::resource('/permissions', 'PermissionsController')->except('show');
        Route::resource('/news', 'NewsController')->except('show');
        Route::resource('/events', 'EventsController')->except('show');
        Route::resource('/forum', 'ForumController')->except('show');
        Route::resource('/glossaries', 'GlossariesController')->except('show');
        Route::resource('/comments', 'CommentsController')->except(['show', 'create', 'store']);
        Route::resource('/bids', 'BidsController')->except(['show', 'create', 'store']);
        Route::resource('/settings', 'SettingsController')->except('show');
        Route::get('/settings/cache-clean', 'SettingsController@cacheClean')->name('settings.cacheClean');

        Route::group(['prefix' => 'survey', 'as' => 'survey.'], function () {
            Route::resource('/questions', 'SurveyQuestionsController')->except('show');
            Route::resource('/factors', 'SurveyFactorsController')->except('show');
            Route::resource('/indicators', 'SurveyIndicatorsController')->except('show');
            Route::resource('/sub-indicators', 'SurveySubIndicatorsController')->except('show');
            Route::resource('/results', 'SurveyResultsController')->only(['index', 'show']);

            Route::get('/indicators/load', 'SurveyIndicatorsController@load')->name('load');
            Route::get('/sub-indicators/load', 'SurveySubIndicatorsController@load')->name('load');
        });
    });
});
