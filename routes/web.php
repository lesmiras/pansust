<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contacts', 'HomeController@contacts')->name('contacts');

Route::post('/feedback', 'HomeController@feedbackStore')->name('feedback.store');

Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/news/{slug}', 'NewsController@show')->name('news.show');
Route::get('/events', 'EventsController@index')->name('events.index');
Route::get('/events/{slug}', 'EventsController@show')->name('events.show');
Route::get('/forum', 'ForumController@index')->name('forum.index');
Route::get('/forum/{slug}', 'ForumController@show')->name('forum.show');
Route::get('/wiki', 'GlossariesController@index')->name('glossaries.index');
Route::get('/wiki/{letter}', 'GlossariesController@show')->name('glossaries.show');

Route::get('/comments/load', 'CommentsController@load')->name('comments.load');
Route::post('/comments', 'CommentsController@store')->name('comments.store');

Route::get('/sat', 'SurveyController@index')->name('survey.index');
Route::get('/sat/load-questions', 'SurveyController@loadQuestions')->name('survey.loadQuestions');
Route::post('/sat/result', 'SurveyController@saveResult')->name('survey.saveResult');
Route::get('/sat/result/{uuid}', 'SurveyController@showResult')->name('survey.showResult');

Route::get('/search', 'SearchController@index')->name('search.index');
Route::get('/search/load', 'SearchController@load')->name('search.load');
