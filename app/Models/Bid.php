<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bid extends Model
{
    use HasFactory;

    const STATUS_NEW = 0;
    const STATUS_CLOSED = 1;

    /**
     * Statuses mapped
     */
    const STATUSES = [
        self::STATUS_NEW    => 'New',
        self::STATUS_CLOSED => 'Closed',
    ];

    protected $guarded = ['id'];

    protected $casts = [
        'body' => 'array',
    ];

    /**
     * Возвращает текстовое значение статуса
     *
     * @return string
     */
    public function getStatusNameAttribute(): string
    {
        return self::STATUSES[$this->status];
    }

    /**
     * Возвращает badge со значением статуса
     *
     * @return string
     */
    public function getStatusBadge(): string
    {
        if ($this->status === self::STATUS_CLOSED) {
            return '<span class="badge badge-primary">Closed</span>';
        }

        return '<span class="badge badge-info">New</span>';
    }
}
