<?php
namespace App\Models;

class SurveyFactor extends Model
{
    protected $guarded = ['id'];

    /**
     * Relationship: survey-indicators
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function indicators()
    {
        return $this->hasMany(SurveyIndicator::class);
    }

    /**
     * Relationship: survey-questions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }
}
