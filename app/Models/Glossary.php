<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Glossary extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
}
