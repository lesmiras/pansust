<?php
namespace App\Models;

class SurveyResult extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'answers'       => 'array',
        'additional'    => 'array',
        'total_factors' => 'array',
    ];
}
