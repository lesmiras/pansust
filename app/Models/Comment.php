<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends Model
{
    use HasFactory;

    public const STATUS_PUBLISHED = 1;
    public const STATUS_HIDDEN = 0;

    /**
     * Statuses
     */
    public const STATUSES = [
        self::STATUS_PUBLISHED => 'Published',
        self::STATUS_HIDDEN    => 'Hidden',
    ];

    protected $guarded = ['id'];

    /**
     * Scope: is_published
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsPublished(Builder $builder)
    {
        $builder->where('status', self::STATUS_PUBLISHED);
    }


    /**
     * Scope: for model
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $article
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFor(Builder $builder, Model $article)
    {
        $builder->where('commentable_id', $article->id)->where('commentable_type', get_class($article));
    }

    /**
     * Relationship: commentable models
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Get badge element with status value
     *
     * @return string
     */
    public function getStatusBadge(): string
    {
        if ($this->status === 1) {
            return '<span class="badge badge-primary">Published</span>';
        }

        return '<span class="badge badge-warning">Hidden</span>';
    }

    /**
     * Get type ClassName from type string
     *
     * @param string $type
     * @return string
     */
    public static function getTypeClassName(string $type): string
    {
        switch ($type) {
            case 'news':
                return News::class;
            case 'event':
                return Event::class;
            case 'forum':
                return Forum::class;
        }

        return '';
    }
}
