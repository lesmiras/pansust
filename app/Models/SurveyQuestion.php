<?php
namespace App\Models;

class SurveyQuestion extends Model
{
    public const TYPE_SINGLE_SELECT = 'single_select';
    public const TYPE_MULTI_SELECT = 'multi_select';
    public const TYPE_INPUT = 'input';

    /**
     * Types mapped
     */
    public const TYPES = [
        self::TYPE_SINGLE_SELECT => 'Select with single choices',
        self::TYPE_MULTI_SELECT  => 'Select with multiple choices',
        self::TYPE_INPUT         => 'Input from text field',
    ];

    protected $guarded = ['id'];

    protected $casts = [
        'answers' => 'array',
    ];

    /**
     * Relationship: survey-factor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function factor()
    {
        return $this->belongsTo(SurveyFactor::class);
    }

    /**
     * Relationship: survey-indicator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicator()
    {
        return $this->belongsTo(SurveyIndicator::class);
    }

    /**
     * Relationship: survey-sub-indicator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subIndicator()
    {
        return $this->belongsTo(SurveySubIndicator::class);
    }

    /**
     * Type is SingleSelect or MultiSelect
     *
     * @return bool
     */
    public static function isSelectableType($type): bool
    {
        return in_array($type, [self::TYPE_SINGLE_SELECT, self::TYPE_MULTI_SELECT]);
    }
}
