<?php
namespace App\Models;

class SurveyIndicator extends Model
{
    protected $guarded = ['id'];

    /**
     * Relationship: survey-sub-indicators
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function factor()
    {
        return $this->belongsTo(SurveyFactor::class);
    }

    /**
     * Relationship: survey-sub-indicators
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subIndicators()
    {
        return $this->hasMany(SurveySubIndicator::class);
    }

    /**
     * Relationship: survey-questions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }
}
