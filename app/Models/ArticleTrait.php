<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @method \Illuminate\Database\Eloquent\Builder isPublished()
 * @method \Illuminate\Database\Eloquent\Builder search()
 */
trait ArticleTrait
{
    /**
     * Scope: search
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch(Builder $builder, $searchQuery)
    {
        $builder->where(function ($query) use ($searchQuery) {
            $query->orWhere('title', 'like', '%' . $searchQuery . '%');
            $query->orWhere('content', 'like', '%' . $searchQuery . '%');
        });
    }

    /**
     * Relationship: comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Дата публикации в формате Y-m-d
     *
     * @return string
     */
    public function getPublishedDateAttribute(): string
    {
        return $this->published_at->format('Y-m-d');
    }

    /**
     * Increments views count
     *
     * @return bool
     */
    public function incrementViews(): bool
    {
        $this->increment('views');

        return $this->save();
    }

    /**
     * Get commentable title
     *
     * @return string
     */
    public function getCommentableTitle(): string
    {
        return $this->title;
    }

    /**
     * Get commentable type
     *
     * @return string
     */
    public function getCommentableType(): string
    {
        return $this->commentableType;
    }

    /**
     * Get preview image or return default
     *
     * @return string
     */
    public function getPreviewImage(): string
    {
        return $this->preview
            ? asset($this->preview)
            : asset('storage/media/preview_default.png');
    }

    /**
     * Get thumbnail image or return default
     *
     * @return string
     */
    public function getThumbnailImage(): string
    {
        if (!$this->preview) {
            return asset('storage/media/thumbnail_black.png');
        }

        $preview = $this->preview;
        $parts = explode('/preview_', $preview);

        if (count($parts) > 1) {
            return $parts[0] . '/thumbnail_' . $parts[1];
        }

        return $parts[0];
    }
}
