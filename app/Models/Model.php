<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    /**
     * Элемент badge со статусом опубликаванности
     *
     * @return string
     */
    public function getIsPublishedBadge(): string
    {
        if ($this->is_published) {
            return '<span class="badge badge-primary">Published</span>';
        }

        return '<span class="badge badge-warning">Hidden</span>';
    }

    /**
     * Scope: is_published
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsPublished(Builder $builder)
    {
        $builder->where('is_published', true);
    }

    /**
     * Get formatted CreatedAt
     *
     * @return string
     */
    public function createdAtFormatted(): string
    {
        return $this->created_at->format('d F Y');
    }
}
