<?php
namespace App\Models;

class SurveySubIndicator extends Model
{
    protected $guarded = ['id'];

    /**
     * Relationship: survey-indicators
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicator()
    {
        return $this->belongsTo(SurveyIndicator::class);
    }

    /**
     * Relationship: survey-questions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }
}
