<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Forum extends Model
{
    use HasFactory, ArticleTrait;

    protected $guarded = ['id'];

    protected $casts = [
        'published_at' => 'datetime',
    ];

    protected $commentableType = 'Forum';

    /**
     * Get link to single article page
     *
     * @return string
     */
    public function getPublicLink(): string
    {
        return route('forum.show', $this->slug);
    }

    /**
     * Get link to edit
     *
     * @return string
     */
    public function getEditLink(): string
    {
        return route('admin.forum.edit', $this->id);
    }
}
