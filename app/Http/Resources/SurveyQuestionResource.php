<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SurveyQuestionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'factor_weight'        => $this->factor->weight,
            'indicator_weight'     => $this->indicator->weight,
            'sub_indicator_weight' => $this->subIndicator->weight,
            'title'                => $this->question,
            'type'                 => $this->type,
            'answers'              => $this->answers,
        ];
    }
}
