<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ForumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title'        => 'required',
            'slug'         => 'required|max:60|alpha_dash|unique:forums,slug',
            'content'      => 'required',
            'published_at' => 'required',
            'preview'      => 'dimensions:min_width=600,min_height=470',
        ];

        if (request()->isMethod('put') || request()->isMethod('patch')) {
            $rules['slug'] = 'required|alpha_dash|unique:forums,slug,' . $this->route('forum');
        } else {
            $rules['preview'] .= '|required';
        }

        return $rules;
    }
}
