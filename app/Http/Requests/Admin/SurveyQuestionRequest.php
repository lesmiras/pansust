<?php
namespace App\Http\Requests\Admin;

use App\Models\SurveyQuestion;
use Illuminate\Foundation\Http\FormRequest;

class SurveyQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'type'             => 'required',
            'question'         => 'required|present',
            'factor_id'        => 'required',
            'indicator_id'     => 'required',
            'sub_indicator_id' => 'required',
            'is_published'     => 'required',
            'multiplier'       => 'nullable|numeric',
        ];

        if (SurveyQuestion::isSelectableType($this->input('type'))) {
            $rules['answers']           = 'required';
            $rules['answers.*.title']   = 'required|present';
            $rules['answers.*.weight']  = 'required|present|numeric';
        } elseif ($this->input('type') === SurveyQuestion::TYPE_INPUT) {
            $rules['answer']           = 'required';
            $rules['answer.weight']    = 'required|present|numeric';
            $rules['answer.type']      = 'required|present';
            $rules['answer.min_value'] = 'nullable|numeric';
            $rules['answer.max_value'] = 'nullable|numeric';
        }

        return $rules;
    }
}
