<?php
namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Forum;
use App\Models\Glossary;
use App\Models\News;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Result page
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $searchQuery = strip_tags($request->input('query'));
        $searchQuery = htmlentities($searchQuery, ENT_QUOTES, 'UTF-8', false);

        if (strlen($searchQuery) < 3) {
            return view('search.empty');
        }

        $news = News::isPublished()
            ->search($searchQuery)
            ->latest()
            ->paginate(5);

        $events = Event::isPublished()
            ->search($searchQuery)
            ->latest()
            ->paginate(5);

        $forums = Forum::isPublished()
            ->search($searchQuery)
            ->latest()
            ->paginate(5);

        $glossaries = Glossary::where(function ($query) use ($searchQuery) {
            $query->orWhere('title', 'like', '%' . $searchQuery . '%');
            $query->orWhere('content', 'like', '%' . $searchQuery . '%');
        })
            ->orderBy('title')
            ->paginate(10);

        $total = $news->total() + $events->total() + $forums->total() + $glossaries->total();

        if ($total == 0) {
            return view('search.empty');
        }

        SEOMeta::setTitle('Search');
        
        return view('search.index', compact(
            'news',
            'events',
            'forums',
            'glossaries',
            'searchQuery',
            'total'
        ));
    }

    /**
     * Load more
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable|array
     */
    public function load(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        if (!in_array($request->input('type'), ['news', 'events', 'forum', 'glossaries'])) {
            abort(404);
        }

        $tabName     = $request->input('type');
        $searchQuery = strip_tags($request->input('query'));
        $searchQuery = htmlentities($searchQuery, ENT_QUOTES, 'UTF-8', false);

        $items = null;
        $html  = null;

        if ($tabName === 'news') {
            $items = News::isPublished()
                ->search($searchQuery)
                ->latest()
                ->paginate(5);

            $view = 'search.item-article';
        } elseif ($tabName === 'events') {
            $items = Event::isPublished()
                ->search($searchQuery)
                ->latest()
                ->paginate(5);

            $view = 'search.item-article';
        } elseif ($tabName === 'forum') {
            $items = Forum::isPublished()
                ->search($searchQuery)
                ->latest()
                ->paginate(5);

            $view = 'search.item-article';
        } elseif ($tabName === 'glossaries') {
            $items = Glossary::where(function ($query) use ($searchQuery) {
                    $query->orWhere('title', 'like', '%' . $searchQuery . '%');
                    $query->orWhere('content', 'like', '%' . $searchQuery . '%');
                })
                ->orderBy('name')
                ->paginate(5);

            $view = 'search.item-glossary';
        }

        if ($items && $items->total()) {
            foreach ($items as $item) {
                $html .= view($view)->with('item', $item)->render();
            }
        }

        return [
            'html'        => $html,
            'total'       => $items ? $items->total() : 0,
            'currentPage' => $items ? $items->currentPage() : 0,
            'lastPage'    => $items ? $items->lastPage() : 0,
        ];
    }
}
