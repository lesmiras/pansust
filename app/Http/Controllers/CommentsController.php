<?php
namespace App\Http\Controllers;

use App\Models\Comment;
use App\Rules\Recaptcha;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Load comments
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable|array
     */
    public function load(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $comments = Comment::isPublished()->latest();

        if ($id = $request->get('id')) {
            $comments = $comments->where('commentable_id', $id);
        }

        if ($type = $request->get('type')) {
            $comments = $comments->where('commentable_type', Comment::getTypeClassName($type));
        }

        $comments = $comments->paginate(3);

        return [
            'html'        => view('comments.comment-list', compact('comments'))->render(),
            'total'       => $comments->total(),
            'currentPage' => $comments->currentPage(),
            'lastPage'    => $comments->lastPage(),
        ];
    }

    /**
     * Store comment
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable|array
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name'      => 'required',
                'email'     => 'required|email',
                'post'      => 'required',
                'comment'   => 'required',
                'id'        => 'required',
                'type'      => 'required',
                'recaptcha' => ['required', 'string', new Recaptcha()]
            ],
            [
                'required'           => 'Field is required.',
                'recaptcha.required' => 'Recaptcha is required.',
                'email'              => 'Must be a valid email address.',
            ]
        );

        $comment = Comment::create([
            'commentable_type' => Comment::getTypeClassName($request->input('type')),
            'commentable_id'   => $request->input('id'),
            'status'           => Comment::STATUS_PUBLISHED,
            'name'             => $request->input('name'),
            'email'            => $request->input('email'),
            'post'             => $request->input('post'),
            'content'          => $request->input('comment'),
        ]);

        return [
            'success' => true,
            'html'    => view('comments.comment-block', compact('comment'))->render(),
            'comment' => $comment->toArray(),
        ];
    }
}
