<?php
namespace App\Http\Controllers;

use App\Http\Resources\SurveyQuestionResource;
use App\Mail\SurveyResultMail;
use App\Models\SurveyQuestion;
use App\Models\SurveyResult;
use App\Services\SurveyService;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mail;

class SurveyController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new SurveyService();
    }

    /**
     * List page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        SEOMeta::setTitle('SAT');

        return view('survey.index');
    }

    /**
     * Save results
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function saveResult(Request $request)
    {
        $uuid         = Str::uuid()->toString();
        $formData     = $request->input('form');
        $userAnswers  = $request->input('answers');
        $factors      = $this->service->getCalculatedFactorsFromAnswers($userAnswers);
        $totalScore   = $this->service->calculateTotalScore($factors);
        $totalFactors = $this->service->calculateTotalFactors($totalScore, $factors);

        SurveyResult::create([
            'uuid'          => $uuid,
            'name'          => $formData['main']['name'],
            'email'         => $formData['main']['email'],
            'affiliation'   => $formData['main']['affiliation'],
            'additional'    => $formData['additional'],
            'answers'       => $userAnswers,
            'total_score'   => $totalScore,
            'total_factors' => $totalFactors,
        ]);

        $emailData = [
            'total_score' => $totalScore,
            'answers'     => $this->service->prepareUserAnswers($userAnswers),
        ];

        try {
            Mail::to($formData['email'])->send(new SurveyResultMail($emailData));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return [
            'redirect' => route('survey.showResult', $uuid)
        ];
    }

    /**
     * Save results
     *
     * @param string $uuid
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showResult(string $uuid)
    {
        $result = SurveyResult::where('uuid', $uuid)->firstOrFail();

        SEOMeta::setTitle('SAT');
        
        return view('survey.result')->with([
            'result' => $result,
        ]);
    }

    /**
     * Load questions
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function loadQuestions()
    {
        $questions = SurveyQuestion::with(['factor', 'indicator', 'subIndicator'])->get();

        return SurveyQuestionResource::collection($questions);
    }
}
