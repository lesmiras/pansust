<?php
namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Forum;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    /**
     * List page
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable|array
     */
    public function index(Request $request)
    {
        $articles = Forum::isPublished();

        if ($request->get('order') === 'popular') {
            $articles = $articles->orderByDesc('views')->latest();
        } else {
            $articles = $articles->latest();
        }

        $articles = $articles->paginate(6);

        if ($request->ajax()) {
            return [
                'html'        => view('forum.articles-list', compact('articles'))->render(),
                'total'       => $articles->total(),
                'currentPage' => $articles->currentPage(),
                'lastPage'    => $articles->lastPage(),
            ];
        }

        SEOMeta::setTitle('Forum');

        return view('forum.index')->with([
            'forums' => $articles
        ]);
    }

    /**
     * Article page
     *
     * @param string $slug
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($slug)
    {
        $article  = Forum::where('slug', $slug)->isPublished()->firstOrFail();
        $comments = Comment::for($article)
            ->isPublished()
            ->latest()
            ->paginate(3);

        $relatedArticles = Forum::isPublished()
            ->where('id', '!=', $article->id)
            ->inRandomOrder()
            ->limit(2)
            ->get();

        $article->incrementViews();

        SEOMeta::setTitle($article->title);

        if ($article->excerpt) {
            SEOMeta::setDescription($article->excerpt);
        }

        return view('forum.show')->with([
            'article'         => $article,
            'comments'        => $comments,
            'relatedArticles' => $relatedArticles,
        ]);
    }
}
