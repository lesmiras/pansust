<?php
namespace App\Http\Controllers;

use App\Models\Glossary;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class GlossariesController extends Controller
{
    /**
     * List page
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $letters    = Glossary::select('letter')->groupBy('letter')->get()->pluck('letter')->toArray();
        $glossaries = [];
        $inSearch   = false;

        if ($search = $request->get('search')) {
            $glossaries = Glossary::where('title', 'like', '%' . $search . '%')->get();
            $inSearch   = true;
        }

        SEOMeta::setTitle('Wikipedia');
        
        return view('glossaries.index', compact('letters', 'glossaries', 'inSearch'));
    }

    /**
     * Letter page
     *
     * @param string $letter
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(string $letter)
    {
        $letters    = Glossary::select('letter')->groupBy('letter')->get()->pluck('letter')->toArray();
        $glossaries = Glossary::where('letter', $letter)->get();

        SEOMeta::setTitle('Wikipedia - ' . $letter);

        return view('glossaries.index', compact('letters', 'glossaries'));
    }
}
