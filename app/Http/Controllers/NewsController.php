<?php
namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\News;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * List page
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable|array
     */
    public function index(Request $request)
    {
        $articles = News::isPublished();

        if ($request->get('order') === 'popular') {
            $articles = $articles->orderByDesc('views')->latest();
        } else {
            $articles = $articles->latest();
        }

        $articles = $articles->paginate(6);

        if ($request->ajax()) {
            return [
                'html'        => view('news.articles-list', compact('articles'))->render(),
                'total'       => $articles->total(),
                'currentPage' => $articles->currentPage(),
                'lastPage'    => $articles->lastPage(),
            ];
        }

        SEOMeta::setTitle('News');

        return view('news.index')->with([
            'news' => $articles
        ]);
    }

    /**
     * Article page
     *
     * @param string $slug
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($slug)
    {
        $article  = News::where('slug', $slug)->isPublished()->firstOrFail();
        $comments = Comment::for($article)->isPublished()->latest()->paginate(3);

        $relatedArticles = News::isPublished()
            ->where('id', '!=', $article->id)
            ->inRandomOrder()
            ->limit(2)
            ->get();

        $article->incrementViews();

        SEOMeta::setTitle($article->title);

        if ($article->excerpt) {
            SEOMeta::setDescription($article->excerpt);
        }

        return view('news.show')->with([
            'article'         => $article,
            'comments'        => $comments,
            'relatedArticles' => $relatedArticles,
        ]);
    }
}
