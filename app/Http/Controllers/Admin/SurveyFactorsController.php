<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SurveyFactor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SurveyFactorsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:survey-manage');
    }

    /**
     * Display a listing of the SurveyFactors.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $factors = SurveyFactor::latest()->get();

        return view('admin.survey.factors.index', compact('factors'));
    }

    /**
     * Show the form for creating a new SurveyFactor.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.survey.factors.create');
    }

    /**
     * Store a newly created SurveyFactors in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'   => 'required',
            'weight' => 'required|numeric',
        ]);

        $factor = SurveyFactor::create([
            'name'   => $request->input('name'),
            'weight' => $request->input('weight'),
        ]);

        return redirect()
            ->route('admin.survey.factors.edit', $factor)
            ->with('success', 'SurveyFactor created successfully');
    }

    /**
     * Show the form for editing the specified SurveyFactor.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $factor = SurveyFactor::findOrFail($id);

        return view('admin.survey.factors.edit', compact('factor'));
    }

    /**
     * Update the specified SurveyFactor in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'   => 'required',
            'weight' => 'required|numeric',
        ]);

        $factor = SurveyFactor::findOrFail($id);
        $factor->update([
            'name'   => $request->input('name'),
            'weight' => $request->input('weight'),
        ]);

        return redirect()
            ->route('admin.survey.factors.edit', $factor)
            ->with('success', 'SurveyFactor updated successfully');
    }

    /**
     * Remove the specified SurveyFactor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        SurveyFactor::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'SurveyFactor deleted successfully');

            return response()->json(['success' => true, 'redirect' => route('admin.survey.factors.index')]);
        } else {
            return redirect()
                ->route('admin.survey.factors..index')
                ->with('success','SurveyFactor deleted successfully');
        }
    }
}
