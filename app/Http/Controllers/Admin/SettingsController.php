<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class SettingsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:settings-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $settings = Setting::latest()->get();

        return view('admin.settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new Setting.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created Setting in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key'   => 'required|alpha_dash|unique:settings,key',
            'value' => 'required',
        ]);

        $setting = Setting::create([
            'key'   => $request->input('key'),
            'value' => $request->input('value'),
        ]);

        return redirect()
            ->route('admin.settings.edit', $setting)
            ->with('success', 'Setting created successfully');
    }

    /**
     * Show the form for editing the specified Setting.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        return view('admin.settings.edit', compact('setting'));
    }

    /**
     * Update the specified Setting in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'key'   => 'required|alpha_dash|unique:settings,key,' . $id,
            'value' => 'required',
        ]);

        $setting = Setting::findOrFail($id);
        $setting->update([
            'key'   => $request->input('key'),
            'value' => $request->input('value'),
        ]);

        return redirect()
            ->route('admin.settings.edit', $setting)
            ->with('success', 'Setting updated successfully');
    }

    /**
     * Remove the specified Setting from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Setting::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Setting deleted successfully');

            return response()->json(['success' => true, 'redirect' => route('admin.settings.index')]);
        } else {
            return redirect()
                ->route('admin.settings.index')
                ->with('success', 'Setting deleted successfully');
        }
    }

    /**
     * Clean cache
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function cacheClean()
    {
        Artisan::call('cache:clear');

        return redirect()
            ->route('admin.settings.index')
            ->with('success', 'Cache cleaned successfully');
    }
}
