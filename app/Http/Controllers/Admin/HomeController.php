<?php
namespace App\Http\Controllers\Admin;

use App\Models\Bid;
use App\Models\Comment;
use App\Models\SurveyResult;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    /**
     * Display dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $surveyTotal   = SurveyResult::count();
        $commentsTotal = Comment::count();
        $bidsTotal     = Bid::count();

        $surveys  = SurveyResult::latest()->limit(5)->get();
        $comments = Comment::latest()->limit(5)->get();
        $bids     = Bid::latest()->limit(5)->get();

        return view('admin.dashboard', compact(
            'surveyTotal',
            'commentsTotal',
            'bidsTotal',
            'surveys',
            'comments',
            'bids'
        ));
    }
}
