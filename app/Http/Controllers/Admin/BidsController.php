<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BidsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:bids-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bids = Bid::latest()->paginate(20);

        return view('admin.bids.index', compact('bids'));
    }

    /**
     * Show the form for editing the specified Bid.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $bid     = Bid::findOrFail($id);
        $statues = Bid::STATUSES;

        return view('admin.bids.edit', compact('bid', 'statues'));
    }

    /**
     * Update the specified Bid in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $bid = Bid::findOrFail($id);
        $bid->update([
            'status' => $request->input('status'),
        ]);

        return redirect()
            ->route('admin.bids.edit', $bid)
            ->with('success', 'Bid updated successfully');
    }

    /**
     * Remove the specified Bid from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Bid::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Bid deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.bids.index')]);
        } else {
            return redirect()
                ->route('admin.bids.index')
                ->with('success','Bid deleted successfully');
        }
    }
}
