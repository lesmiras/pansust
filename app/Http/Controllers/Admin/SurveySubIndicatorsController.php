<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SurveyFactor;
use App\Models\SurveyIndicator;
use App\Models\SurveySubIndicator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SurveySubIndicatorsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:survey-manage');
    }

    /**
     * Display a listing of the SurveySubIndicators.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $subIndicators = SurveySubIndicator::with('indicator')
            ->orderBy('indicator_id')
            ->orderBy('name')
            ->latest()
            ->get();

        return view('admin.survey.sub-indicators.index', compact('subIndicators'));
    }

    /**
     * Display a listing of the SurveyIndicators.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function load(Request $request)
    {
        $subIndicators = SurveySubIndicator::query();

        if ($request->has('indicator_id')) {
            $subIndicators = $subIndicators->where('indicator_id', $request->get('indicator_id'));
        }

        $subIndicators = $subIndicators->orderBy('name')->get();

        return $subIndicators->map(function ($item) {
            return [
                'id'   => $item->id,
                'name' => $item->name,
            ];
        });
    }

    /**
     * Show the form for creating a new SurveySubIndicator.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $indicators = SurveyIndicator::get()->pluck('name', 'id');

        return view('admin.survey.sub-indicators.create', compact('indicators'));
    }

    /**
     * Store a newly created SurveySubIndicators in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'weight'       => 'required',
            'indicator_id' => 'required',
        ]);

        $subIndicator = SurveySubIndicator::create([
            'name'         => $request->input('name'),
            'weight'       => $request->input('weight'),
            'indicator_id' => $request->input('indicator_id'),
        ]);

        return redirect()
            ->route('admin.survey.sub-indicators.edit', $subIndicator)
            ->with('success', 'SurveySubIndicator created successfully');
    }

    /**
     * Show the form for editing the specified SurveySubIndicator.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $indicators   = SurveyIndicator::get()->pluck('name', 'id');
        $subIndicator = SurveySubIndicator::findOrFail($id);

        return view('admin.survey.sub-indicators.edit', compact('subIndicator', 'indicators'));
    }

    /**
     * Update the specified SurveySubIndicator in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         => 'required',
            'weight'       => 'required',
            'indicator_id' => 'required',
        ]);

        $subIndicator = SurveySubIndicator::findOrFail($id);
        $subIndicator->update([
            'name'         => $request->input('name'),
            'weight'       => $request->input('weight'),
            'indicator_id' => $request->input('indicator_id'),
        ]);

        return redirect()
            ->route('admin.survey.sub-indicators.edit', $subIndicator)
            ->with('success', 'SurveySubIndicator updated successfully');
    }

    /**
     * Remove the specified SurveySubIndicator from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        SurveySubIndicator::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'SurveySubIndicator deleted successfully');

            return response()->json(['success' => true, 'redirect' => route('admin.survey.sub-indicators.index')]);
        } else {
            return redirect()
                ->route('admin.survey.sub-indicators.index')
                ->with('success','SurveySubIndicator deleted successfully');
        }
    }
}
