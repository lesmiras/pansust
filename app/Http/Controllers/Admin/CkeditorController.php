<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \Illuminate\Routing\Controller;

class CkeditorController extends Controller
{
    /**
     * Upload images action
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName   = pathinfo($originName, PATHINFO_FILENAME);
            $extension  = $request->file('upload')->getClientOriginalExtension();
            $fileName   = $fileName . '_' . time() . '.' . $extension;

            $request->file('upload')->storeAs('images', $fileName, 'public');

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url             = asset('/storage/images/' . $fileName);
            $msg             = 'Image uploaded successfully';
            $response        = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
}
