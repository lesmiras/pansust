<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SurveyQuestionRequest;
use App\Models\SurveyFactor;
use App\Models\SurveyIndicator;
use App\Models\SurveyQuestion;
use App\Models\SurveySubIndicator;
use Illuminate\Support\Facades\Session;

class SurveyQuestionsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:survey-manage');
    }

    /**
     * Display a listing of the SurveyQuestions.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $questions = SurveyQuestion::with(['factor', 'indicator', 'subIndicator'])->latest()->get();

        return view('admin.survey.questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new SurveyQuestion.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $factors       = SurveyFactor::latest()->get()->pluck('name', 'id');
        $indicators    = old('factor_id')
            ? SurveyIndicator::where('factor_id', old('factor_id'))->latest()->get()->pluck('name', 'id')
            : [];
        $subIndicators = old('indicator_id')
            ? SurveySubIndicator::where('indicator_id', old('indicator_id'))->latest()->get()->pluck('name', 'id')
            : [];
        $questionType  = old('type');
        $answerList    = SurveyQuestion::isSelectableType($questionType) && old('answers')
            ? old('answers')
            : [];
        $answerData    = $questionType === SurveyQuestion::TYPE_INPUT && old('answer')
            ? old('answer')
            : [];

        return view('admin.survey.questions.create', compact(
            'factors',
            'indicators',
            'subIndicators',
            'questionType',
            'answerList',
            'answerData'
        ));
    }

    /**
     * Store a newly created SurveyQuestions in storage.
     *
     * @param  \App\Http\Requests\Admin\SurveyQuestionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SurveyQuestionRequest $request)
    {
        $question = SurveyQuestion::create([
            'type'             => $request->input('type'),
            'question'         => $request->input('question'),
            'factor_id'        => $request->input('factor_id'),
            'indicator_id'     => $request->input('indicator_id'),
            'sub_indicator_id' => $request->input('sub_indicator_id'),
            'is_published'     => $request->input('is_published'),
            'multiplier'       => $request->input('multiplier'),
            'answers'          => $this->getAnswerValue($request),
        ]);

        return redirect()
            ->route('admin.survey.questions.edit', $question)
            ->with('success', 'SurveyQuestion created successfully');
    }

    /**
     * Show the form for editing the specified SurveyQuestion.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $question      = SurveyQuestion::findOrFail($id);
        $factors       = SurveyFactor::latest()->get()->pluck('name', 'id');
        $indicators    = SurveyIndicator::where('factor_id', $question->factor_id)
            ->latest()->get()->pluck('name', 'id');
        $subIndicators = SurveySubIndicator::where('indicator_id', $question->indicator_id)
            ->latest()->get()->pluck('name', 'id');
        $questionType  = old('type') ?? $question->type;
        $answerList    = SurveyQuestion::isSelectableType($questionType)
            ? old('answers') ?? (!isset($question->answers['weight']) ? $question->answers : [])
            : [];
        $answerData    = $questionType === SurveyQuestion::TYPE_INPUT
            ? old('answer') ?? $question->answers
            : [];

        return view('admin.survey.questions.edit', compact(
            'question',
            'factors',
            'indicators',
            'subIndicators',
            'questionType',
            'answerData',
            'answerList'
        ));
    }

    /**
     * Update the specified SurveyQuestion in storage.
     *
     * @param \App\Http\Requests\Admin\SurveyQuestionRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SurveyQuestionRequest $request, $id)
    {
        $question = SurveyQuestion::findOrFail($id);
        $question->update([
            'type'             => $request->input('type'),
            'question'         => $request->input('question'),
            'factor_id'        => $request->input('factor_id'),
            'indicator_id'     => $request->input('indicator_id'),
            'sub_indicator_id' => $request->input('sub_indicator_id'),
            'is_published'     => $request->input('is_published'),
            'multiplier'       => $request->input('multiplier'),
            'answers'          => $this->getAnswerValue($request),
        ]);

        return redirect()
            ->route('admin.survey.questions.edit', $question)
            ->with('success', 'SurveyQuestion updated successfully');
    }

    /**
     * Remove the specified SurveyQuestion from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        SurveyQuestion::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'SurveyQuestion deleted successfully');

            return response()->json(['success' => true, 'redirect' => route('admin.survey.questions.index')]);
        } else {
            return redirect()
                ->route('admin.survey.questions..index')
                ->with('success','SurveyQuestion deleted successfully');
        }
    }

    /**
     * Get answers data from request.
     *
     * @param \App\Http\Requests\Admin\SurveyQuestionRequest $request
     * @return array
     */
    private function getAnswerValue(SurveyQuestionRequest $request): array
    {
        if (SurveyQuestion::isSelectableType($request->input('type'))) {
            $result =  array_map(function ($item) {
                return [
                    'id'     => (int) $item['id'],
                    'title'  => $item['title'],
                    'weight' => (float) $item['weight'],
                ];
            }, $request->input('answers'));
        } else {
            $result = [
                'weight'    => (float) $request->input('answer')['weight'],
                'type'      => $request->input('answer')['type'],
                'min_value' => $request->input('answer')['min_value'] != ''
                    ? (float) $request->input('answer')['min_value']
                    : null,
                'max_value' => $request->input('answer')['max_value'] != ''
                    ? (float) $request->input('answer')['max_value']
                    : null,
            ];
        }

        return $result;
    }
}
