<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use Illuminate\Support\Facades\Session;

class PermissionsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:permissions-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $permissions = Permission::latest()->get();

        return view('admin.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new Permission.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.permissions.create');
    }

    /**
     * Store a newly created Permission in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|alpha_dash|unique:permissions,name',
        ]);

        $permission = Permission::create([
            'name' => $request->input('name'),
        ]);

        return redirect()
            ->route('admin.permissions.edit', $permission)
            ->with('success', 'Permission created successfully');
    }

    /**
     * Show the form for editing the specified Permission.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);

        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified Permission in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|alpha_dash|unique:permissions,name,' . $id,
        ]);

        $permission = Permission::findOrFail($id);
        $permission->name = $request->name;
        $permission->update();

        return redirect()
            ->route('admin.permissions.edit', $permission)
            ->with('success', 'Permission updated successfully');
    }

    /**
     * Remove the specified Permission from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Permission::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Permission deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.permissions.index')]);
        } else {
            return redirect()
                ->route('admin.permissions.index')
                ->with('success', 'Permission deleted successfully');
        }
    }
}
