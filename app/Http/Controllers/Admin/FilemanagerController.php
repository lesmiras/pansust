<?php
namespace App\Http\Controllers\Admin;

use \Illuminate\Routing\Controller;

class FilemanagerController extends Controller
{
    /**
     * Display index page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.filemanager');
    }
}
