<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Glossary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GlossariesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:glossaries-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $glossaries = Glossary::latest()->paginate(20);

        return view('admin.glossaries.index', compact('glossaries'));
    }

    /**
     * Show the form for creating a new Glossary.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.glossaries.create');
    }

    /**
     * Store a newly created Glossary in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'   => 'required|unique:glossaries,title',
            'content' => 'required',
        ]);

        $title    = trim($request->input('title'));
        $glossary = Glossary::create([
            'title'   => $title,
            'letter'  => mb_substr($title, 0, 1),
            'content' => $request->input('content'),
        ]);

        return redirect()
            ->route('admin.glossaries.edit', $glossary)
            ->with('success', 'Glossary created successfully');
    }

    /**
     * Show the form for editing the specified Glossary.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $glossary = Glossary::findOrFail($id);

        return view('admin.glossaries.edit', compact('glossary'));
    }

    /**
     * Update the specified Glossary in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'   => 'required|unique:glossaries,title,' . $id,
            'content' => 'required',
        ]);

        $title    = trim($request->input('title'));
        $glossary = Glossary::findOrFail($id);
        $glossary->update([
            'title'   => $title,
            'letter'  => mb_substr($title, 0, 1),
            'content' => $request->input('content'),
        ]);

        return redirect()
            ->route('admin.glossaries.edit', $glossary)
            ->with('success', 'Glossary updated successfully');
    }

    /**
     * Remove the specified Glossary from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Glossary::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Glossary deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.glossaries.index')]);
        } else {
            return redirect()
                ->route('admin.glossaries.index')
                ->with('success','Glossary deleted successfully');
        }
    }
}
