<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Event;
use App\Models\Forum;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CommentsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:comments-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $commentable = null;

        if ($request->has('id') && $request->has('type')) {
            switch ($request->type) {
                case 'news':
                    $commentable = News::find($request->id);
                    break;
                case 'event':
                    $commentable = Event::find($request->id);
                    break;
                case 'forum':
                    $commentable = Forum::find($request->id);
                    break;
            }
        }

        $comments = Comment::with('commentable')->latest();

        if ($commentable) {
            $comments->where('commentable_id', $commentable->id)->where('commentable_type', get_class($commentable));
        }

        $comments = $comments->paginate(20)->withQueryString();

        return view('admin.comments.index', compact('comments', 'commentable'));
    }

    /**
     * Show the form for editing the specified Comment.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $comment = Comment::with('commentable')->findOrFail($id);

        return view('admin.comments.edit', compact('comment'));
    }

    /**
     * Update the specified Comment in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required',
            'post'    => 'required',
            'content' => 'required',
        ]);

        $comment = Comment::findOrFail($id);
        $comment->update([
            'name'    => $request->input('name'),
            'email'   => $request->input('email'),
            'post'    => $request->input('post'),
            'content' => $request->input('content'),
            'status'  => $request->input('status'),
        ]);

        $comment->update();

        return redirect()
            ->route('admin.comments.edit', $comment)
            ->with('success', 'Comment updated successfully');
    }

    /**
     * Remove the specified Comment from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Comment::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Comment deleted successfully');

            return response()->json(['success' => true, 'redirect' => route('admin.comments.index')]);
        } else {
            return redirect()
                ->route('admin.comments.index')
                ->with('success','Comment deleted successfully');
        }
    }
}
