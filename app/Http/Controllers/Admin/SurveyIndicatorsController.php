<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SurveyFactor;
use App\Models\SurveyIndicator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SurveyIndicatorsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:survey-manage');
    }

    /**
     * Display a listing of the SurveyIndicators.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $indicators = SurveyIndicator::with('factor')
            ->orderBy('factor_id')
            ->orderBy('name')
            ->latest()
            ->get();

        return view('admin.survey.indicators.index', compact('indicators'));
    }

    /**
     * Display a listing of the SurveyIndicators.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function load(Request $request)
    {
        $indicators = SurveyIndicator::query();

        if ($request->has('factor_id')) {
            $indicators = $indicators->where('factor_id', $request->get('factor_id'));
        }

        $indicators = $indicators->orderBy('name')->get();

        return $indicators->map(function ($item) {
            return [
                'id'   => $item->id,
                'name' => $item->name,
            ];
        });
    }

    /**
     * Show the form for creating a new SurveyIndicator.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $factors = SurveyFactor::get()->pluck('name', 'id');

        return view('admin.survey.indicators.create', compact('factors'));
    }

    /**
     * Store a newly created SurveyIndicators in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'weight'    => 'required|numeric',
            'factor_id' => 'required',
        ]);

        $indicator = SurveyIndicator::create([
            'name'      => $request->input('name'),
            'weight'    => $request->input('weight'),
            'factor_id' => $request->input('factor_id'),
        ]);

        return redirect()
            ->route('admin.survey.indicators.edit', $indicator)
            ->with('success', 'SurveyIndicator created successfully');
    }

    /**
     * Show the form for editing the specified SurveyIndicator.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $factors   = SurveyFactor::get()->pluck('name', 'id');
        $indicator = SurveyIndicator::findOrFail($id);

        return view('admin.survey.indicators.edit', compact('indicator', 'factors'));
    }

    /**
     * Update the specified SurveyIndicator in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required',
            'weight'    => 'required|numeric',
            'factor_id' => 'required',
        ]);

        $indicator = SurveyIndicator::findOrFail($id);
        $indicator->update([
            'name'      => $request->input('name'),
            'weight'    => $request->input('weight'),
            'factor_id' => $request->input('factor_id'),
        ]);

        return redirect()
            ->route('admin.survey.indicators.edit', $indicator)
            ->with('success', 'SurveyIndicator updated successfully');
    }

    /**
     * Remove the specified SurveyIndicator from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        SurveyIndicator::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'SurveyIndicator deleted successfully');

            return response()->json(['success' => true, 'redirect' => route('admin.survey.indicators.index')]);
        } else {
            return redirect()
                ->route('admin.survey.indicators.index')
                ->with('success','SurveyIndicator deleted successfully');
        }
    }
}
