<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventRequest;
use App\Models\Event;
use App\Services\ArticleService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class EventsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:events-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $events = Event::withCount('comments')->latest()->paginate(20);

        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new Event.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.events.create');
    }

    /**
     * Store a newly created Event in storage.
     *
     * @param  \App\Http\Requests\Admin\EventRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EventRequest $request)
    {
        $event = Event::create([
            'title'        => $request->input('title'),
            'slug'         => $request->input('slug'),
            'is_published' => $request->input('is_published'),
            'published_at' => $request->input('published_at'),
            'excerpt'      => $request->input('excerpt'),
            'content'      => $request->input('content'),
        ]);

        if ($request->hasFile('preview')) {
            $event->preview = (new ArticleService())->uploadPreviewImage($request->file('preview'), 'events/' . $event->id);
        }

        $event->update();

        return redirect()
            ->route('admin.events.edit', $event)
            ->with('success', 'Event created successfully');
    }

    /**
     * Show the form for editing the specified Event.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.events.edit', compact('event'));
    }

    /**
     * Update the specified Event in storage.
     *
     * @param \App\Http\Requests\Admin\EventRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EventRequest $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->fill([
            'title'        => $request->input('title'),
            'slug'         => $request->input('slug'),
            'is_published' => $request->input('is_published'),
            'published_at' => $request->input('published_at'),
            'excerpt'      => $request->input('excerpt'),
            'content'      => $request->input('content'),
        ]);

        if ($request->hasFile('preview')) {
            $event->preview = (new ArticleService())->uploadPreviewImage($request->file('preview'), 'events/' . $event->id);
        }

        $event->update();

        return redirect()
            ->route('admin.events.edit', $event)
            ->with('success', 'Event updated successfully');
    }

    /**
     * Remove the specified Event from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        File::deleteDirectory(storage_path('public/events/' . $id));
        Event::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Event deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.events.index')]);
        } else {
            return redirect()
                ->route('admin.events.index')
                ->with('success','Event deleted successfully');
        }
    }
}
