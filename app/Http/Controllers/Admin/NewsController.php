<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;
use App\Services\ArticleService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class NewsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:news-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $news = News::withCount('comments')->latest()->paginate(20);

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new News.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created News in storage.
     *
     * @param  \App\Http\Requests\Admin\NewsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsRequest $request)
    {
        $news = News::create([
            'title'        => $request->input('title'),
            'slug'         => $request->input('slug'),
            'is_published' => $request->input('is_published'),
            'published_at' => $request->input('published_at'),
            'excerpt'      => $request->input('excerpt'),
            'content'      => $request->input('content'),
        ]);

        if ($request->hasFile('preview')) {
            $news->preview = (new ArticleService())->uploadPreviewImage($request->file('preview'), 'news/' . $news->id);
        }

        $news->update();

        return redirect()
            ->route('admin.news.edit', $news)
            ->with('success', 'News created successfully');
    }

    /**
     * Show the form for editing the specified News.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified News in storage.
     *
     * @param \App\Http\Requests\Admin\NewsRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsRequest $request, $id)
    {
        $news = News::findOrFail($id);
        $news->fill([
            'title'        => $request->input('title'),
            'slug'         => $request->input('slug'),
            'is_published' => $request->input('is_published'),
            'published_at' => $request->input('published_at'),
            'excerpt'      => $request->input('excerpt'),
            'content'      => $request->input('content'),
        ]);

        if ($request->hasFile('preview')) {
            $news->preview = (new ArticleService())->uploadPreviewImage($request->file('preview'), 'news/' . $id);
        }

        $news->update();

        return redirect()
            ->route('admin.news.edit', $news)
            ->with('success', 'News updated successfully');
    }

    /**
     * Remove the specified News from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        File::deleteDirectory(storage_path('public/news/' . $id));
        News::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'News deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.news.index')]);
        } else {
            return redirect()
                ->route('admin.news.index')
                ->with('success','News deleted successfully');
        }
    }
}
