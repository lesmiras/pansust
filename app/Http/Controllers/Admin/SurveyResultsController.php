<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SurveyResult;
use App\Services\SurveyService;

class SurveyResultsController extends Controller
{
    private $service;

    function __construct()
    {
        $this->middleware('permission:survey-manage');

        $this->service = new SurveyService();
    }

    /**
     * Display a listing of the SurveyResults.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $results = SurveyResult::latest()->paginate(20);

        return view('admin.survey.results.index', compact('results'));
    }

    /**
     * Show the specified SurveyResult.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $result        = SurveyResult::findOrFail($id);
        $userAnswers   = $this->service->prepareUserAnswers($result->answers);
        $subIndicators = $this->service->calculateSubIndicators($userAnswers);
        $indicators    = $this->service->calculateIndicators($subIndicators);
        $factors       = $this->service->calculateFactors($indicators);

        return view('admin.survey.results.show', compact('result', 'factors'));
    }
}
