<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ForumRequest;
use App\Models\Forum;
use App\Services\ArticleService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class ForumController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:forums-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $forums = Forum::withCount('comments')->latest()->paginate(20);

        return view('admin.forum.index', compact('forums'));
    }

    /**
     * Show the form for creating a new Forum.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.forum.create');
    }

    /**
     * Store a newly created Forum in storage.
     *
     * @param  \App\Http\Requests\Admin\ForumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ForumRequest $request)
    {
        $forum = Forum::create([
            'title'        => $request->input('title'),
            'slug'         => $request->input('slug'),
            'is_published' => $request->input('is_published'),
            'published_at' => $request->input('published_at'),
            'excerpt'      => $request->input('excerpt'),
            'content'      => $request->input('content'),
        ]);

        if ($request->hasFile('preview')) {
            $forum->preview = (new ArticleService())->uploadPreviewImage($request->file('preview'), 'forums/' . $forum->id);
        }

        $forum->update();

        return redirect()
            ->route('admin.forum.edit', $forum)
            ->with('success', 'Forum created successfully');
    }

    /**
     * Show the form for editing the specified Forum.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $forum = Forum::findOrFail($id);

        return view('admin.forum.edit', compact('forum'));
    }

    /**
     * Update the specified Forum in storage.
     *
     * @param \App\Http\Requests\Admin\ForumRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ForumRequest $request, $id)
    {
        $forum = Forum::findOrFail($id);
        $forum->fill([
            'title'        => $request->input('title'),
            'slug'         => $request->input('slug'),
            'is_published' => $request->input('is_published'),
            'published_at' => $request->input('published_at'),
            'excerpt'      => $request->input('excerpt'),
            'content'      => $request->input('content'),
        ]);

        if ($request->hasFile('preview')) {
            $forum->preview = (new ArticleService())->uploadPreviewImage($request->file('preview'), 'forums/' . $forum->id);
        }

        $forum->update();

        return redirect()
            ->route('admin.forum.edit', $forum)
            ->with('success', 'Forum updated successfully');
    }

    /**
     * Remove the specified Forum from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        File::deleteDirectory(storage_path('public/forums/' . $id));
        Forum::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Forum article deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.forum.index')]);
        } else {
            return redirect()
                ->route('admin.forum.index')
                ->with('success','Forum article deleted successfully');
        }
    }
}
