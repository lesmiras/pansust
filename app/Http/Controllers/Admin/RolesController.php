<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Session;

class RolesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:roles-manage');
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $roles = Role::latest()->get();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $permissions = Permission::all()->pluck('name','id');

        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|unique:roles,name',
            'permissions' => 'required|array',
        ]);

        $role = Role::create([
            'name' => $request->input('name')
        ]);

        $role->syncPermissions($request->input('permissions'));

        return redirect()
            ->route('admin.roles.edit', $role)
            ->with('success', 'Role created successfully');
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $role        = Role::findOrFail($id);
        $permissions = Permission::all()->pluck('name','id');

        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified Role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required|unique:roles,name,' . $id,
            'permissions' => 'required|array',
        ]);

        $role = Role::findOrFail($id);
        $role->name = $request->input('name');
        $role->update();

        $role->syncPermissions($request->input('permissions'));

        return redirect()
            ->route('admin.roles.edit', $role)
            ->with('success', 'Role updated successfully');
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Role::findOrFail($id)->delete();

        if (request()->ajax()) {
            Session::flash('success', 'Role deleted successfull');

            return response()->json(['success' => true, 'redirect' => route('admin.roles.index')]);
        } else {
            return redirect()
                ->route('admin.roles.index')
                ->with('success','Role deleted successfully');
        }
    }
}
