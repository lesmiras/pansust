<?php
namespace App\Http\Controllers;

use App\Models\Bid;
use App\Models\Event;
use App\Models\Forum;
use App\Models\News;
use App\Rules\Recaptcha;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Home page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $latestNews   = News::isPublished()->latest()->limit(3)->get();
        $latestEvents = Event::isPublished()->latest()->limit(3)->get();
        $latestForums = Forum::isPublished()->latest()->limit(6)->get();

        SEOMeta::setTitle('Home');

        return view('home')->with([
            'news'   => $latestNews,
            'events' => $latestEvents,
            'forums' => $latestForums,
        ]);
    }

    /**
     * About page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        SEOMeta::setTitle('About us');

        return view('about');
    }

    /**
     * Contacts page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contacts()
    {
        SEOMeta::setTitle('Contact us');

        return view('contacts');
    }

    /**
     * Store data from feedback forms
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function feedbackStore(Request $request)
    {
        $this->validate(
            $request,
            [
                'name'        => 'required',
                'email'       => 'required|email',
                'comment'     => 'sometimes|required',
                'affiliation' => 'required',
                'occupancy'   => 'required',
                'recaptcha'   => ['required', 'string', new Recaptcha()]
            ],
            [
                'required' => 'Field is required.',
                'email'    => 'Must be a valid email address.',
            ]
        );

        $body = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'comment' => $request->input('comment'),
            'affiliation' => $request->input('affiliation'),
            'occupancy' => $request->input('occupancy'),
        ];

        Bid::create([
            'name' => $request->input('formName'),
            'type' => $request->input('type', 'feedback'),
            'body' => $body,
            'status' => Bid::STATUS_NEW,
        ]);

        if ($request->ajax()) {
            return response()->json(['success' => true]);
        } else {
            return redirect()->back()->with('success', 'Form saved');
        }
    }
}
