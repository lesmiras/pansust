<?php
namespace App\Http\Middleware;

class AdminPanelAccess
{
    public function handle($request, \Closure $next)
    {
        if (!\Auth::guest() && \Auth::user()->can('admin-panel-access')) {
            return $next($request);
        }

        return redirect()->route('admin.home');
    }
}
