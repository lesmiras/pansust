<?php
namespace App\Services;

use App\Models\SurveyFactor;
use App\Models\SurveyIndicator;
use App\Models\SurveyQuestion;
use App\Models\SurveySubIndicator;

class SurveyService
{
    /**
     * Get calculated factors from user answers
     *
     * @param array $userAnswers
     * @return array
     */
    public function getCalculatedFactorsFromAnswers(array $userAnswers): array
    {
        $userAnswers   = $this->prepareUserAnswers($userAnswers);
        $subIndicators = $this->calculateSubIndicators($userAnswers);
        $indicators    = $this->calculateIndicators($subIndicators);

        return $this->calculateFactors($indicators);
    }

    /**
     * Get calculated total_factors
     *
     * @param array $factors
     * @return array
     */
    public function calculateTotalFactors(float $totalScore, array $factors): array
    {
        $factorENV = [];
        $factorECO = [];
        $factorSF  = [];

        foreach ($factors as $factor) {
            switch ($factor['name']) {
                case 'ENV':
                    $factorENV = [
                        'weight' => $factor['weight'],
                        'score'  => $factor['score'],
                    ];
                    break;
                case 'ECO':
                    $factorECO = [
                        'weight' => $factor['weight'],
                        'score'  => $factor['score'],
                    ];
                    break;
                case 'S&F':
                    $factorSF = [
                        'weight' => $factor['weight'],
                        'score'  => $factor['score'],
                    ];
                    break;
            }
        }

        $factorsCalc = ($factorENV['weight'] * $factorENV['score'])
            + ($factorECO['weight'] * $factorECO['score'])
            + ($factorSF['weight'] * $factorSF['score']);
        $totalENV    = $totalScore * ($factorENV['weight'] * $factorENV['score']) / $factorsCalc;
        $totalECO    = $totalScore * ($factorECO['weight'] * $factorECO['score']) / $factorsCalc;
        $totalSF     = $totalScore * ($factorSF['weight'] * $factorSF['score']) / $factorsCalc;

        return [
            'env' => round($totalENV, 2),
            'eco' => round($totalECO, 2),
            'sf'  => round($totalSF, 2),
        ];
    }

    /**
     * Get calculated total_score from prepared factors
     *
     * @param array $factors
     * @return float
     */
    public function calculateTotalScore(array $factors): float
    {
        return round(array_sum(array_column($factors, 'score_sum')) / array_sum(array_column($factors, 'weight')), 2);
    }

    /**
     * Prepare user answers with question
     *
     * @param array $userAnswers
     * @return array
     */
    public function prepareUserAnswers(array $userAnswers): array
    {
        $questions = SurveyQuestion::latest()->get();

        foreach ($userAnswers as &$userAnswer) {
            $answer       = [];
            $userAnswerId = $userAnswer['answer'];
            $question     = $questions->where('id', $userAnswer['question_id'])->first();

            if ($question->type === SurveyQuestion::TYPE_SINGLE_SELECT) {
                $answer = current(array_filter($question->answers, function ($item) use ($userAnswerId) {
                    return $item['id'] == $userAnswerId;
                }));
            } else if ($question->type === SurveyQuestion::TYPE_MULTI_SELECT) {
                $items = array_values(array_filter($question->answers, function ($item) use ($userAnswerId) {
                    return in_array($item['id'], $userAnswerId);
                }));

                $answer = [
                    'weight' => array_sum(array_column($items, 'weight')),
                    'items'  => $items,
                ];
            } else if ($question->type === SurveyQuestion::TYPE_INPUT) {
                $answer['title']  = $userAnswerId;
                $answer['weight'] = (float) $userAnswerId * $question->answers['weight'];
            }

            $userAnswer['answer']           = $answer;
            $userAnswer['question']         = $question->only(['question', 'type', 'answers']);
            $userAnswer['sub_indicator_id'] = $question->sub_indicator_id;
        }

        return $userAnswers;
    }

    /**
     * Calculate and prepare sub-indicators from prepared user answers
     *
     * @param array $userAnswers
     * @return array
     */
    public function calculateSubIndicators(array $userAnswers): array
    {
        $subIndicators = SurveySubIndicator::orderBy('indicator_id')->orderBy('name')->get()->toArray();

        // temporary save weights for ECO1.2
        $eco12WeightSum = 0;
        // temporary save answers for S&F5.1
        $sf51Answers = [];
        // temporary save answers for S&F5.2
        $sf52Answers = [];
        // temporary save answers for S&F5.3
        $sf53Answers = [];

        // SUB-INDICATORS score calculation
        foreach ($subIndicators as &$subIndicator) {
            $subIndicatorQuestions = array_values(array_filter($userAnswers, function ($item) use ($subIndicator) {
                return $item['sub_indicator_id'] === $subIndicator['id'];
            }));

            $sumWeight = 0;

            if ($subIndicatorQuestions) {
                foreach ($subIndicatorQuestions as $subIndicatorQuestion) {
                    if ($subIndicator['name'] === 'S&F5.1') {
                        $sf51Answers[] = $subIndicatorQuestion['answer']['weight'];
                    } elseif ($subIndicator['name'] === 'S&F5.2') {
                        $sf52Answers[] = $subIndicatorQuestion['answer']['weight'];
                    } elseif ($subIndicator['name'] === 'S&F5.3') {
                        $sf53Answers[] = $subIndicatorQuestion['answer']['weight'];
                    } else {
                        $sumWeight += $subIndicatorQuestion['answer']['weight'];
                    }
                }
            }

            $subIndicator['questions'] = $subIndicatorQuestions;
            $subIndicator['score']     = round($sumWeight * $subIndicator['weight'], 2);

            // weight saves for ECO1.2 from another sub-indicators
            if (in_array($subIndicator['name'], ['ENV1.1', 'ENV4.1', 'ECO1.1'])) {
                $eco12WeightSum += $sumWeight;
            }

            // special score calculation for 'S&F5.1'
            if ($subIndicator['name'] == 'S&F5.1') {
                $sf51Score = 5*5/$sf51Answers[0] + 5*$sf51Answers[1]/3;

                if ($sf51Score > 10) {
                    $sf51Score = 10;
                } else {
                    $sf51Score = floor($sf51Score);
                }

                $subIndicator['score'] = round($subIndicator['weight'] * $sf51Score, 2);
            }

            // special score calculation for 'S&F5.2'
            if ($subIndicator['name'] == 'S&F5.2') {
                $sf52Score = (
                    ($sf52Answers[0] * 20)
                    + $sf52Answers[1]
                    + ($sf52Answers[2] * 25)
                    + ($sf52Answers[3] * 3)
                    + ($sf52Answers[4] * 20)
                    + ($sf52Answers[5] * 8)
                    + ($sf52Answers[6] * 3)
                ) / 7;

                if ($sf52Score > 5.5) {
                    $sf52Score = 0;
                } elseif ($sf52Score > 3.5 && $sf52Score < 5.5) {
                    $sf52Score = 3;
                } elseif ($sf52Score > 2.5 && $sf52Score < 3.5) {
                    $sf52Score = 5;
                } elseif ($sf52Score > 1 && $sf52Score < 2.5) {
                    $sf52Score = 8;
                } else {
                    $sf52Score = 10;
                }

                $subIndicator['score'] = round($subIndicator['weight'] * $sf52Score, 2);
            }

            // special score calculation for 'S&F5.3'
            if ($subIndicator['name'] == 'S&F5.3') {
                $sf53Score = $sf53Answers[0] / $sf53Answers[1];

                if ($sf53Score > 0.9) {
                    $sf53Score = 10;
                } elseif ($sf53Score > 0.7 && $sf53Score < 0.9) {
                    $sf53Score = 8;
                } elseif ($sf53Score > 0.4 && $sf53Score < 0.7) {
                    $sf53Score = 5;
                } elseif ($sf53Score > 0.2 && $sf53Score < 0.4) {
                    $sf53Score = 3;
                } elseif ($sf53Score > 0.1 && $sf53Score < 0.2) {
                    $sf53Score = 1;
                } else {
                    $sf53Score = 0;
                }

                $subIndicator['score'] = round($subIndicator['weight'] * $sf53Score, 2);
            }
        }

        foreach ($subIndicators as &$subIndicator) {
            // special score calculation for 'ECO1.2'
            if ($subIndicator['name'] == 'ECO1.2') {
                $subIndicator['score'] = round($subIndicator['weight'] * ($eco12WeightSum / 3), 2);
            }
        }

        return $subIndicators;
    }

    /**
     * Calculate and prepare indicators from prepared sub-indicators
     *
     * @param array $subIndicators
     * @return array
     */
    public function calculateIndicators(array $subIndicators): array
    {
        $indicators = SurveyIndicator::orderBy('factor_id')->orderBy('name')->get()->toArray();

        foreach ($indicators as &$indicator) {
            $indicatorSubIndicators = array_values(array_filter($subIndicators, function ($item) use ($indicator) {
                return $item['indicator_id'] == $indicator['id'];
            }));

            $sumScore  = 0;
            $sumWeight = 0;

            if ($indicatorSubIndicators) {
                foreach ($indicatorSubIndicators as $subIndicatorRow) {
                    $sumScore  += $subIndicatorRow['score'];
                    $sumWeight += $subIndicatorRow['weight'];
                }
            }

            if ($sumScore && $sumWeight) {
                $indicator['score'] = round($sumScore / $sumWeight, 2);
            } else {
                $indicator['score'] = 0;
            }

            $indicator['sub_indicators'] = $indicatorSubIndicators;
        }

        return $indicators;
    }

    /**
     * Calculate and prepare factors from prepared indicators
     *
     * @param array $indicators
     * @return array
     */
    public function calculateFactors(array $indicators): array
    {
        $factors = SurveyFactor::orderBy('name')->get()->toArray();

        foreach ($factors as &$factor) {
            $factorIndicators = array_filter($indicators, function ($item) use ($factor) {
                return $item['factor_id'] == $factor['id'];
            });

            $sumScore  = 0;
            $sumWeight = 0;

            if ($factorIndicators) {
                foreach ($factorIndicators as $indicatorRow) {
                    if (!isset($indicatorRow['score'])) {
                        continue;
                    }

                    $sumScore  += $indicatorRow['score'] * $indicatorRow['weight'];
                    $sumWeight += $indicatorRow['weight'];
                }
            }

            if ($sumScore && $sumWeight) {
                $factor['score'] = round($sumScore / $sumWeight, 2);
            } else {
                $factor['score'] = 0;
            }

            $factor['indicators'] = $factorIndicators;
            $factor['score_sum']  = $factor['score'] * $factor['weight'];
        }

        return $factors;
    }
}