<?php
namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ArticleService
{
    /**
     * Upload preview images and resize to origin and thumbnail
     *
     * @param \Illuminate\Http\UploadedFile
     * @param string $modelFolder
     * @return string
     */
    public function uploadPreviewImage(UploadedFile $originalImage, string $modelFolder): string
    {
        $storageFolder = storage_path('app/public/' . $modelFolder);

        File::deleteDirectory($storageFolder);

        if (!File::isDirectory($storageFolder)) {
            File::makeDirectory($storageFolder, 0777, true, true);
        }

        $tempImage = Image::make($originalImage);
        $tempImage->backup();

        $imageName = uniqid() . '.' . $originalImage->getClientOriginalExtension();

        $tempImage->fit(200, 200);
        $tempImage->save($storageFolder . '/thumbnail_' . $imageName);

        $tempImage->reset();

        $tempImage->fit(600, 470);
        $tempImage->save($storageFolder . '/preview_' . $imageName);

        return '/storage/' . $modelFolder . '/preview_' . $imageName;
    }
}