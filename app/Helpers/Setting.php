<?php
namespace App\Helpers;

use App\Models\Setting as SettingModel;
use Illuminate\Support\Facades\Cache;

class Setting
{
    /**
     * Set value against a key.
     *
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function set($key, $value)
    {
        $setting = SettingModel::create(
            [
                'key' => $key,
                'value' => $value,
            ]
        );

        return $setting ? $value : false;
    }

    /**
     * Get value by a key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return Cache::remember(
            'setting_' . $key,
            86400,
            function () use ($key) {
                $setting = SettingModel::where('key', $key)->first();

                return $setting ? $setting->value : false;
            });
    }
}
